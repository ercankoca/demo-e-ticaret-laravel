@extends('layouts.main')

@section('content')

<div role="main" class="main">
<div class="container">
	<ol class="breadcrumb">
    	<li class="breadcrumb-item"><a href="/">Anasayfa</a></li>
    	<li class="breadcrumb-item"><a href="/mobilya">Mobilya</a></li>
    	<li class="breadcrumb-item"><a href="/mobilya/gardiroplar">Gardiroplar</a></li>
    	<li class="breadcrumb-item active">Sürgülü Gardiroplar</li>
	</ol>
			
<div class="row">
	<div class="col-md-3"></div>
	<div class="col-md-9 shop">
				<ul class="products product-thumb-info-list">
									<li class="col-sm-3 col-xs-12 product">
										<a href="shop-product-sidebar.html">
											<span class="onsale">Sale!</span>
										</a>
										<span class="product-thumb-info">
											<a href="shop-cart.html" class="add-to-cart-product">
												<span><i class="fa fa-shopping-cart"></i> Sepete Ekle</span>
											</a>
											<a href="/gardiroplar/surgulu-gardiroplar/surgulu-gardiroplar-urun1">
												<span class="product-thumb-info-image">
													<span class="product-thumb-info-act">
														<span class="product-thumb-info-act-left"><em>View</em></span>
														<span class="product-thumb-info-act-right"><em><i class="fa fa-plus"></i> Details</em></span>
													</span>
													<img alt="" class="img-responsive" src="/frontend/img/categories/surgulu-gardiroplar_yeni.jpg">
												</span>
											</a>
											<span class="product-thumb-info-content">
												<a href="shop-product-sidebar.html">
													<span class="surgu">Sürgülü Gardıroplar</span>
													<span class="price">
														<del><span class="amount">$325</span></del>
														<ins><span class="amount">$299</span></ins>
													</span>
												</a>
											</span>
										</span>
									</li>
									<li class="col-sm-3 col-xs-12 product">
										<span class="product-thumb-info">
											<a href="shop-cart.html" class="add-to-cart-product">
												<span><i class="fa fa-shopping-cart"></i> Sepete Ekle</span>
											</a>
											<a href="shop-product-sidebar.html">
												<span class="product-thumb-info-image">
													<span class="product-thumb-info-act">
														<span class="product-thumb-info-act-left"><em>View</em></span>
														<span class="product-thumb-info-act-right"><em><i class="fa fa-plus"></i> Details</em></span>
													</span>
													<img alt="" class="img-responsive" src="/frontend/img/categories/surgulu-gardiroplar_yeni.jpg">
												</span>
											</a>
											<span class="product-thumb-info-content">
												<a href="shop-product-sidebar.html">
													<span>Sürgülü Gardıroplar</span>
													<span class="price">
														<span class="amount">$72</span>
													</span>
												</a>
											</span>
										</span>
									</li>
									<li class="col-sm-3 col-xs-12 product">
										<span class="product-thumb-info">
											<a href="shop-cart.html" class="add-to-cart-product">
												<span><i class="fa fa-shopping-cart"></i> Sepete Ekle</span>
											</a>
											<a href="shop-product-sidebar.html">
												<span class="product-thumb-info-image">
													<span class="product-thumb-info-act">
														<span class="product-thumb-info-act-left"><em>View</em></span>
														<span class="product-thumb-info-act-right"><em><i class="fa fa-plus"></i> Details</em></span>
													</span>
													<img alt="" class="img-responsive" src="/frontend/img/categories/surgulu-gardiroplar_yeni.jpg">
												</span>
											</a>
											<span class="product-thumb-info-content">
												<a href="shop-product-sidebar.html">
													<span>Sürgülü Gardıroplar</span>
													<span class="price">
														<span class="amount">$60</span>
													</span>
												</a>
											</span>
										</span>
									</li>
									<li class="col-sm-3 col-xs-12 product">
										<span class="product-thumb-info">
											<a href="shop-cart.html" class="add-to-cart-product">
												<span><i class="fa fa-shopping-cart"></i> Sepete Ekle</span>
											</a>
											<a href="shop-product-sidebar.html">
												<span class="product-thumb-info-image">
													<span class="product-thumb-info-act">
														<span class="product-thumb-info-act-left"><em>View</em></span>
														<span class="product-thumb-info-act-right"><em><i class="fa fa-plus"></i> Details</em></span>
													</span>
													<img alt="" class="img-responsive" src="/frontend/img/categories/surgulu-gardiroplar_yeni.jpg">
												</span>
											</a>
											<span class="product-thumb-info-content">
												<a href="shop-product-sidebar.html">
													<span>Sürgülü Gardıroplar</span>
													<span class="price">
														<span class="amount">$199</span>
													</span>
												</a>
											</span>
										</span>
									</li>
								</ul>


		
	</div>
</div>
</div>

</div>

@endsection('content')

@section("js")


@endsection
