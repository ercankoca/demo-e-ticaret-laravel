@extends('layouts.main')

@section('content')

<div role="main" class="main">			

		<div class="container">					
						
					<ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="#">Anasayfa</a></li>
				    <li class="breadcrumb-item"><a href="#">Mobilya</a></li>
				    <li class="breadcrumb-item active">Gardiroplar</li>
					</ol>										
					

					<div class="row">
						<div class="col-md-4 ">
						
						
							<aside class="sidebar">

								<h4>Gardıroplar</h4>
								<ul class="nav nav-list primary push-bottom">
									<li><a href="/mobilya/gardiroplar/surgulu-gardiroplar">Sürgülü Gardıroplar</a></li>
									<li><a href="/mobilya/gardiroplar/kapakli-gardiroplar">Kapaklı Gardıroplar</a></li>
									<li><a href="/mobilya/gardiroplar/bez-gardiroplar">Bez Dolaplar</a></li>
									
								</ul>

								<div class="tabs">
									<ul class="nav nav-tabs">
										<li class="active"><a href="#popularPosts" data-toggle="tab"><i class="fa fa-star"></i> Popular</a></li>
										<li><a href="#recentPosts" data-toggle="tab">Recent</a></li>
									</ul>
									<div class="tab-content">
										<div class="tab-pane active" id="popularPosts">
											<ul class="simple-post-list">
												<li>
													<div class="post-image">
														<div class="img-thumbnail">
															<a href="blog-post.html">
																<img src="/frontend/img/blog/blog-thumb-1.jpg" alt="">
															</a>
														</div>
													</div>
													<div class="post-info">
														<a href="blog-post.html">Nullam Vitae Nibh Un Odiosters</a>
														<div class="post-meta">
															 Jan 10, 2013
														</div>
													</div>
												</li>
												<li>
													<div class="post-image">
														<div class="img-thumbnail">
															<a href="blog-post.html">
																<img src="/frontend/img/blog/blog-thumb-2.jpg" alt="">
															</a>
														</div>
													</div>
													<div class="post-info">
														<a href="blog-post.html">Vitae Nibh Un Odiosters</a>
														<div class="post-meta">
															 Jan 10, 2013
														</div>
													</div>
												</li>
												<li>
													<div class="post-image">
														<div class="img-thumbnail">
															<a href="blog-post.html">
																<img src="/frontend/img/blog/blog-thumb-3.jpg" alt="">
															</a>
														</div>
													</div>
													<div class="post-info">
														<a href="blog-post.html">Odiosters Nullam Vitae</a>
														<div class="post-meta">
															 Jan 10, 2013
														</div>
													</div>
												</li>
											</ul>
										</div>
										<div class="tab-pane" id="recentPosts">
											<ul class="simple-post-list">
												<li>
													<div class="post-image">
														<div class="img-thumbnail">
															<a href="blog-post.html">
																<img src="/frontend/img/blog/blog-thumb-2.jpg" alt="">
															</a>
														</div>
													</div>
													<div class="post-info">
														<a href="blog-post.html">Vitae Nibh Un Odiosters</a>
														<div class="post-meta">
															 Jan 10, 2013
														</div>
													</div>
												</li>
												<li>
													<div class="post-image">
														<div class="img-thumbnail">
															<a href="blog-post.html">
																<img src="/frontend/img/blog/blog-thumb-3.jpg" alt="">
															</a>
														</div>
													</div>
													<div class="post-info">
														<a href="blog-post.html">Odiosters Nullam Vitae</a>
														<div class="post-meta">
															 Jan 10, 2013
														</div>
													</div>
												</li>
												<li>
													<div class="post-image">
														<div class="img-thumbnail">
															<a href="blog-post.html">
																<img src="/frontend/img/blog/blog-thumb-1.jpg" alt="">
															</a>
														</div>
													</div>
													<div class="post-info">
														<a href="blog-post.html">Nullam Vitae Nibh Un Odiosters</a>
														<div class="post-meta">
															 Jan 10, 2013
														</div>
													</div>
												</li>
											</ul>
										</div>
									</div>
								</div>


							</aside>
						
						
					
					
					</div>


					<div class="col-md-8">
			            
			        <div class="row shop">

								<ul class="products product-thumb-info-list">
									<li class="col-sm-3 col-xs-12 product hoverBorder"  >
										
										<span class="product-thumb-info "   >
											
											<a href="/mobilya/gardiroplar/surgulu-gardiroplar" >
												
													
													<img alt="" class="img-responsive "  src="/frontend/img/categories/surgulu-gardiroplar_yeni.jpg">
												
											</a>
											<span class="product-thumb-info-content">
												<a href="shop-product-sidebar.html" >
													<span>Sürgülü Gardıroplar</span>
												
												</a>
											</span>
										</span>
									</li>
									<li class="col-sm-3 col-xs-12 product hoverBorder">
										<span class="product-thumb-info">	

											<a href="/mobilya/gardiroplar/kapakli-gardiroplar">							
												
													<img alt="" class="img-responsive" src="/frontend/img/categories/kapakli-dolaplar.jpg">
												
											</a>
											<span class="product-thumb-info-content">
												<a href="shop-product-sidebar.html">
													<span>Kapaklı Gardıroplar</span>
													
												</a>
											</span>
										</span>
									</li>
									<li class="col-sm-3 col-xs-12 product hoverBorder">
										<span class="product-thumb-info">
											
											<a href="/mobilya/gardiroplar/bez-gardiroplar">										
												
													<img alt="" class="img-responsive" src="/frontend/img/categories/bez-dolaplar.jpg">
												
											</a>
											<span class="product-thumb-info-content">
												<a href="shop-product-sidebar.html">
													<span>Bez Dolaplar</span>
													
												</a>
											</span>
										</span>
									</li>
									<li class="col-sm-3 col-xs-12 product hoverBorder">
										<span class="product-thumb-info">
										
											<a href="shop-product-sidebar.html">
												
												
													<img alt="" class="img-responsive" src="/frontend/img/products/product-4.jpg">
												
											</a>
											<span class="product-thumb-info-content">
												<a href="shop-product-sidebar.html">
													<span>Luxury bag</span>
												
												</a>
											</span>
										</span>
									</li>
								</ul>

							

             

						</div>
					</div>

					<hr class="tall" />

					<div class="row">

						<div class="col-md-12">
							<h3>Related <strong>Work</strong></h3>
						</div>

						<ul class="portfolio-list">
							<li class="col-md-3 col-xs-6">
								<div class="portfolio-item thumbnail">
									<a href="portfolio-single-project.html" class="thumb-info">
										<img alt="" class="img-responsive" src="/frontend/img/projects/project.jpg">
										<span class="thumb-info-title">
											<span class="thumb-info-inner">SEO</span>
											<span class="thumb-info-type">Website</span>
										</span>
										<span class="thumb-info-action">
											<span title="Universal" class="thumb-info-action-icon"><i class="fa fa-link"></i></span>
										</span>
									</a>
								</div>
							</li>
							<li class="col-md-3 col-xs-6">
								<div class="portfolio-item thumbnail">
									<a href="portfolio-single-project.html" class="thumb-info">
										<img alt="" class="img-responsive" src="/frontend/img/projects/project-1.jpg">
										<span class="thumb-info-title">
											<span class="thumb-info-inner">Okler</span>
											<span class="thumb-info-type">Brand</span>
										</span>
										<span class="thumb-info-action">
											<span title="Universal" class="thumb-info-action-icon"><i class="fa fa-link"></i></span>
										</span>
									</a>
								</div>
							</li>
							<li class="col-md-3 col-xs-6">
								<div class="portfolio-item thumbnail">
									<a href="portfolio-single-project.html" class="thumb-info">
										<img alt="" class="img-responsive" src="/frontend/img/projects/project-2.jpg">
										<span class="thumb-info-title">
											<span class="thumb-info-inner">The Fly</span>
											<span class="thumb-info-type">Logo</span>
										</span>
										<span class="thumb-info-action">
											<span title="Universal" class="thumb-info-action-icon"><i class="fa fa-link"></i></span>
										</span>
									</a>
								</div>
							</li>
							<li class="col-md-3 col-xs-6">
								<div class="portfolio-item thumbnail">
									<a href="portfolio-single-project.html" class="thumb-info">
										<img alt="" class="img-responsive" src="/frontend/img/projects/project-3.jpg">
										<span class="thumb-info-title">
											<span class="thumb-info-inner">The Code</span>
											<span class="thumb-info-type">Website</span>
										</span>
										<span class="thumb-info-action">
											<span title="Universal" class="thumb-info-action-icon"><i class="fa fa-link"></i></span>
										</span>
									</a>
								</div>
							</li>
						</ul>

					</div>

				</div>

			</div>

			<section class="call-to-action featured footer">
				<div class="container">
					<div class="row">
						<div class="center">
							<h3>Porto is <strong>everything</strong> you need to create an <strong>awesome</strong> website! <a href="http://themeforest.net/item/porto-responsive-html5-template/4106987" target="_blank" class="btn btn-lg btn-primary" data-appear-animation="bounceIn">Buy Now!</a> <span class="arrow hlb hidden-xs hidden-sm hidden-md" data-appear-animation="rotateInUpLeft" style="top: -22px;"></span></h3>
						</div>
					</div>
				</div>
			</section>

@endsection('content')

@section("js")


@endsection