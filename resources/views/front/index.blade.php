@extends('layouts.main')


@section('content')

		<div role="main" class="main">
			<div id="carouselBlk">
	<div id="myCarousel" class="carousel slide">
		<div class="carousel-inner">
		  <div class="item active">
		  <div class="container">
			<a href="register.html"><img style="width:100%" src="/frontend/themes/images/carousel/1.png" alt="special offers"/></a>
			<div class="carousel-caption">
				  <h4>Second Thumbnail label</h4>
				  <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
				</div>
		  </div>
		  </div>
		  <div class="item">
		  <div class="container">
			<a href="register.html"><img style="width:100%" src="/frontend/themes/images/carousel/2.png" alt=""/></a>
				<div class="carousel-caption">
				  <h4>Second Thumbnail label</h4>
				  <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
				</div>
		  </div>
		  </div>
		  <div class="item">
		  <div class="container">
			<a href="register.html"><img src="/frontend/themes/images/carousel/3.png" alt=""/></a>
			<div class="carousel-caption">
				  <h4>Second Thumbnail label</h4>
				  <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
				</div>
			
		  </div>
		  </div>
		   <div class="item">
		   <div class="container">
			<a href="register.html"><img src="/frontend/themes/images/carousel/4.png" alt=""/></a>
			<div class="carousel-caption">
				  <h4>Second Thumbnail label</h4>
				  <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
				</div>
		   
		  </div>
		  </div>
		   <div class="item">
		   <div class="container">
			<a href="register.html"><img src="/frontend/themes/images/carousel/5.png" alt=""/></a>
			<div class="carousel-caption">
				  <h4>Second Thumbnail label</h4>
				  <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
			</div>
		  </div>
		  </div>
		   <div class="item">
		   <div class="container">
			<a href="register.html"><img src="/frontend/themes/images/carousel/6.png" alt=""/></a>
			<div class="carousel-caption">
				  <h4>Second Thumbnail label</h4>
				  <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
				</div>
		  </div>
		  </div>
		</div>
		<a class="left carousel-control" href="#myCarousel" data-slide="prev">&lsaquo;</a>
		<a class="right carousel-control" href="#myCarousel" data-slide="next">&rsaquo;</a>
		
	  </div> 
</div>


				
				<div class="home-intro" id="home-intro">
					<div class="container">
				
						<div class="row">
							<div class="col-md-8">
								<p>
									The fastest way to grow your business with the leader in <em>Technology</em>
									<span>Check out our options and features included.</span>
								</p>
							</div>
							<div class="col-md-4">
								<div class="get-started">
									<a href="#" class="btn btn-lg btn-primary">Get Started Now!</a>
									<div class="learn-more">or <a href="index.html">learn more.</a></div>
								</div>
							</div>
						</div>
				
					</div>
				</div>
				
				<div class="container">
				
					<div class="row center">
						<div class="col-md-12">
							<h1 class="short word-rotator-title">
								Porto is
								<strong class="inverted">
									<span class="word-rotate" data-plugin-options='{"delay": 2000, "animDelay": 300}'>
										<span class="word-rotate-items">
											<span>incredibly</span>
											<span>especially</span>
											<span>extremely</span>
										</span>
									</span>
								</strong>
								beautiful and fully responsive.
							</h1>
							<p class="featured lead">
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce elementum, nulla vel pellentesque consequat, ante nulla hendrerit arcu, ac tincidunt mauris lacus sed leo. vamus suscipit molestie vestibulum.
							</p>
						</div>
					</div>
				
				</div>
				
				<div class="home-concept">
					<div class="container">
				
						

									<div class="container">

				

					<div class="row">

						<ul class="portfolio-list sort-destination" >
					
							<li class="col-md-3 col-sm-6 col-xs-12 isotope-item logos">
								<div class="portfolio-item img-thumbnail">
									<a href="portfolio-single-project.html" class="thumb-info">
										<img alt="" class="img-responsive" src="/frontend/img/projects/ikea-restorani.jpg">
										<span class="thumb-info-title">
											<span class="thumb-info-inner">IKEA Aile Kart</span>
											<span class="thumb-info-type">IKEA Aile Kart sahiplerine özel %20 indirimli ürün fırsatlarını kaçırmayın.</span>
										</span>
										<span class="thumb-info-action">
											<span title="Universal" class="thumb-info-action-icon"><i class="fa fa-link"></i></span>
										</span>
									</a>
								</div>
							</li>
							<li class="col-md-3 col-sm-6 col-xs-12 isotope-item websites">
								<div class="portfolio-item img-thumbnail">
									<a href="portfolio-single-project.html" class="thumb-info">
										<img alt="" class="img-responsive" src="/frontend/img/projects/ikea-restorani.jpg">
										<span class="thumb-info-title">
											<span class="thumb-info-inner">IKEA Restoran</span>
											<span class="thumb-info-type">Sizleri IKEA Restoranlarına bekliyoruz.</span>
										</span>
										<span class="thumb-info-action">
											<span title="Universal" class="thumb-info-action-icon"><i class="fa fa-link"></i></span>
										</span>
									</a>
								</div>
							</li>
							<li class="col-md-3 col-sm-6 col-xs-12 isotope-item websites">
								<div class="portfolio-item img-thumbnail">
									<a href="portfolio-single-project.html" class="thumb-info">
										<img alt="" class="img-responsive" src="/frontend/img/projects/ikea-restorani.jpg">
										<span class="thumb-info-title">
											<span class="thumb-info-inner">IKEA Kurumsal Satış</span>
											<span class="thumb-info-type">İş yeri alışverişlerinizde size sunduğumuz hizmetleri inceleyin.</span>
										</span>
										<span class="thumb-info-action">
											<span title="Universal" class="thumb-info-action-icon"><i class="fa fa-link"></i></span>
										</span>
									</a>
								</div>
							</li>
							<li class="col-md-3 col-sm-6 col-xs-12 isotope-item websites">
								<div class="portfolio-item img-thumbnail">
									<a href="portfolio-single-project.html" class="thumb-info">
										<img alt="" class="img-responsive" src="/frontend/img/projects/ikea-restorani.jpg">
										<span class="thumb-info-title">
											<span class="thumb-info-inner">Ortak evimiz Dünya’yı koruyalım.</span>
											<span class="thumb-info-type">TEMA Vakfı ile geliştirdiğimiz sosyal sorumluluk projesinin detayları için tıklayın.</span>
										</span>
										<span class="thumb-info-action">
											<span title="Universal" class="thumb-info-action-icon"><i class="fa fa-link"></i></span>
										</span>
									</a>
								</div>
							</li>
						
						</ul>
					</div>

				</div>
						
						
				
					</div>
				</div>
				
				<div class="container">
				
					<div class="row">
						<hr class="tall" />
					</div>

						<div class="row" style="">
					
						<div class="col-md-12">
							<h2 style="font-weight: bold">Yatak odaları gardırop sistemlerinde hediye çeki fırsatı!</h2>

							
								<div class="col-md-12">
									
										<img class="img-responsive" src="/frontend/img/pax-platsa--ana-sayfa-1-tr.jpg" width="100%">
										
								</div>

								<div class="col-md-12">
									<p class="tall">PAX ve PLATSA gardırop sistemleri ve dolap içi düzenleyicileri ile hayalinizdeki yatak odası düzenine kavuşabilirsiniz. Her gün giymek için bir şeyler aramak yerine, en sevdiğiniz pantolonun, tişörtün, kemerin ve aksesuarların tam olarak nerede olduğunu bilebilirsiniz.</p>
								</div>
						</div>
					</div>				
				</div>


							<div class="container">
				
					<div class="row">
						<hr class="tall" />
					</div>

						<div class="row" style="">
					<p class="tall" style="font-weight: bold">29 Haziran - 2 Eylül 2018 tarihleri yatak odası gardırop sistemlerinde her 1.000 TL’lik alışverişe 100 TL hediye çeki fırsatını kaçırmayın!</p>
						<div class="col-lg-5">
										<img class="img-responsive" src="/frontend/img/pax-platsa--ana-sayfa-gif.gif" width="430px" height="100%">
						</div>

							<div class="col-lg-7">
										<img class="img-responsive" src="/frontend/img/pax-platsa--ana-sayfa-2.jpg" width="610px" height="410px">
								
						</div>
					</div>
				</div>

							<div class="container">
				
					<div class="row">
						<hr class="tall" />
					</div>

						<div class="row" style="">
					<h2 style="font-weight: bold">Dolabınızı kendiniz tasarlayın.</h2>
						<div class="col-lg-12">
										<img class="img-responsive" src="/frontend/img/pax-platsa--ana-sayfa-3.jpg" width="1060px" height="360px">
										<p style="position:absolute; left:750px; top:105px; width:250px; color: black;">Moda ve zevkler zamanla değişebilir. Ancak giyim ve yaşam tarzınız değişse bile akıllı ve esnek çözümler sunan PAX gardırobunuzu yıllarca kullanabilirsiniz. PAX serisinin iskelet ve kapakları ile KOMPLEMENT dolap içi düzenleme serisinin parçalarını birleştirerek dolabınızı ihtiyacınıza ve zevkinize göre tasarlayabilirsiniz.</p></div><div class="grid grid_4" style="margin-bottom:40px;">
						</div>

					</div>
				</div>
				
				<div class="container">
				
				
				
					<hr class="tall" />
				
					<div class="row center">
						<div class="col-md-12">
							<h2 class="short word-rotator-title">
								We're not the only ones
								<strong>
									<span class="word-rotate" data-plugin-options='{"delay": 3500, "animDelay": 400}'>
										<span class="word-rotate-items">
											<span>excited</span>
											<span>happy</span>
										</span>
									</span>
								</strong>
								about Porto Template...
							</h2>
							<h4 class="lead tall">5,500 customers in 100 countries use Porto Template. Meet our customers.</h4>
						</div>
					</div>
					<div class="row center">
						<div class="owl-carousel" data-plugin-options='{"items": 6, "autoplay": true, "autoplayTimeout": 3000}'>
							<div>
								<img class="img-responsive" src="/frontend/img/logos/logo-1.png" alt="">
							</div>
							<div>
								<img class="img-responsive" src="/frontend/img/logos/logo-2.png" alt="">
							</div>
							<div>
								<img class="img-responsive" src="/frontend/img/logos/logo-3.png" alt="">
							</div>
							<div>
								<img class="img-responsive" src="/frontend/img/logos/logo-4.png" alt="">
							</div>
							<div>
								<img class="img-responsive" src="/frontend/img/logos/logo-5.png" alt="">
							</div>
							<div>
								<img class="img-responsive" src="/frontend/img/logos/logo-6.png" alt="">
							</div>
							<div>
								<img class="img-responsive" src="/frontend/img/logos/logo-4.png" alt="">
							</div>
							<div>
								<img class="img-responsive" src="/frontend/img/logos/logo-2.png" alt="">
							</div>
						</div>
					</div>
				
				</div>
				
				<div class="map-section">
					<section class="featured footer map">
						<div class="container">
							<div class="row">
								<div class="col-md-6">
									<div class="recent-posts push-bottom">
										<h2>Latest <strong>Blog</strong> Posts</h2>
										<div class="row">
											<div class="owl-carousel" data-plugin-options='{"items": 1}'>
												<div>
													<div class="col-md-6">
														<article>
															<div class="date">
																<span class="day">15</span>
																<span class="month">Jan</span>
															</div>
															<h4><a href="blog-post.html">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</a></h4>
															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat libero. <a href="/" class="read-more">read more <i class="fa fa-angle-right"></i></a></p>
														</article>
													</div>
													<div class="col-md-6">
														<article>
															<div class="date">
																<span class="day">15</span>
																<span class="month">Jan</span>
															</div>
															<h4><a href="blog-post.html">Lorem ipsum dolor</a></h4>
															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat. <a href="/" class="read-more">read more <i class="fa fa-angle-right"></i></a></p>
														</article>
													</div>
												</div>
												<div>
													<div class="col-md-6">
														<article>
															<div class="date">
																<span class="day">12</span>
																<span class="month">Jan</span>
															</div>
															<h4><a href="blog-post.html">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</a></h4>
															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat libero. <a href="/" class="read-more">read more <i class="fa fa-angle-right"></i></a></p>
														</article>
													</div>
													<div class="col-md-6">
														<article>
															<div class="date">
																<span class="day">11</span>
																<span class="month">Jan</span>
															</div>
															<h4><a href="blog-post.html">Lorem ipsum dolor</a></h4>
															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. <a href="/" class="read-more">read more <i class="fa fa-angle-right"></i></a></p>
														</article>
													</div>
												</div>
												<div>
													<div class="col-md-6">
														<article>
															<div class="date">
																<span class="day">15</span>
																<span class="month">Jan</span>
															</div>
															<h4><a href="blog-post.html">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</a></h4>
															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat libero. <a href="/" class="read-more">read more <i class="fa fa-angle-right"></i></a></p>
														</article>
													</div>
													<div class="col-md-6">
														<article>
															<div class="date">
																<span class="day">15</span>
																<span class="month">Jan</span>
															</div>
															<h4><a href="blog-post.html">Lorem ipsum dolor</a></h4>
															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat. <a href="/" class="read-more">read more <i class="fa fa-angle-right"></i></a></p>
														</article>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<h2><strong>What</strong> Client’s Say</h2>
									<div class="row">
										<div class="owl-carousel push-bottom" data-plugin-options='{"items": 1}'>
											<div>
												<div class="col-md-12">
													<blockquote class="testimonial">
													<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat.  Donec hendrerit vehicula est, in consequat.  Donec hendrerit vehicula est, in consequat.</p>
													</blockquote>
													<div class="testimonial-arrow-down"></div>
													<div class="testimonial-author">
														<div class="img-thumbnail img-thumbnail-small">
															<img src="img/clients/client-1.jpg" alt="">
														</div>
														<p><strong>John Smith</strong><span>CEO & Founder - Okler</span></p>
													</div>
												</div>
											</div>
											<div>
												<div class="col-md-12">
													<blockquote class="testimonial">
													<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
													</blockquote>
													<div class="testimonial-arrow-down"></div>
													<div class="testimonial-author">
														<div class="img-thumbnail img-thumbnail-small">
															<img src="/frontend/img/clients/client-1.jpg" alt="">
														</div>
														<p><strong>John Smith</strong><span>CEO & Founder - Okler</span></p>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
				</div>
			</div>
 

@endsection




@section("js")


@endsection