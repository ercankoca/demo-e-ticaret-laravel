@extends('admin.layout.admin')

@section('content')

	<h3>Resim Ekle</h3>
   
      <div class="row">       
            @foreach($products as $product)
            <div class="col-md-12" style="width: 100%; padding-bottom: 20px">
            
            <form action="/panel/product/image-upload/{{$product->id}}" method="POST" class="dropzone" id="my-awesome-dropzone-{{$product->id}}">
              {{csrf_field()}}

             </form>

            </div>


              
             
        @endforeach

          

        <div class="col-md-12">

            <div class="box" style="height: 800px; width:100%">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <thead>
                            <th>Önizleme</th>
                            <th>isActive</th>
                            <th>isCover</th>
                            <th class="col-md-2">İşlemler</th>
                        </thead>
                        <tbody class="sortableList" postUrl="room/roomImageRankUpdate">
                     @foreach($product->images as $image) 
                            <tr id="">
                                <td>
                                    <a class="thumbnail fancybox" rel="ligthbox" href="{{$image->image_path}}">
                                        <img
                                            width="80"
                                            src="{{$image->image_path}}"
                                            alt=""
                                            class="img-responsive"
                                        />
                                    </a>

                                </td>
                                <td>
                                    <input class = "toggle_check"
                                           data-onstyle="success"
                                           data-size = "mini"
                                           data-on="Aktif"
                                           data-off="Pasif"
                                           data-offstyle="danger"
                                           type="checkbox"
                                           data-toggle="toggle"
                                           dataID="<?php echo $image->id; ?>"
                                     <?php echo ($image->isActive == 1) ? "checked" : ""; ?>   
                                    />
                                </td>
                                <td>
                                    <input class = "toggle_check"
                                           data-onstyle="success"
                                           data-size = "mini"
                                           data-on="Aktif"
                                           data-off="Pasif"
                                           data-offstyle="danger"
                                           type="checkbox"
                                           data-toggle="toggle"
                                           dataID="<?php echo $image->id; ?>"
                                     <?php echo ($image->isCover == 1) ? "checked" : ""; ?>   
                                    />
                                </td>
                                <td>
                                    <a href="deleteImage/{{$image->id}}">
                                        <i class="fa fa-trash" style="font-size:16px;"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
                <!-- /.box-body -->
         
        </div>
    </div>
    
    <button id="deneme" type="button">tıkla</button>
              

@endsection

@section('js')
    <script src="/admin/js/jquery.fancybox.min.js" type="text/javascript"></script>
    <script src="/admin/js/bootbox.min.js" type="text/javascript" ></script>
    <script src="/admin/js/bootstrap-toggle.min.js" type="text/javascript" ></script>

<script>
       $(document).ready(function(){
        $('.deneme').click(function(){
            alert('fff');
        })
        //FANCYBOX
        //https://github.com/fancyapps/fancyBox
        $(".fancybox").fancybox({
            openEffect: "none",
            closeEffect: "none"
        });
    });
    
          // Bootstrap Toggle init
        $('.toggle_check').bootstrapToggle();

        // isActive Change

        $('.toggle_check').change(function () {

            alert('fgfd');

        });


    });
</script>

@endsection

@section('css')

<link rel="stylesheet" type="text/css" href="/admin/css/jquery.fancybox.min.css">
<link rel="stylesheet" type="text/css" href="/admin/css/bootstrap-toggle.min.css">
<style type="text/css">
       .dropzone{
        border: 2px dashed #d4d4d4;
        height: auto;
        background-color: #fff;
        color: black;
        font-size: 20px;
        font-style: italic;
    }

    .gallery
    {
        display: inline-block;
        margin-top: 20px;
    }

    .thumbnail {
        width: 90px;
        height:90px;
    }
</style>

@endsection
    
    
    