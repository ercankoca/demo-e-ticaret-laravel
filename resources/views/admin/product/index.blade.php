@extends('admin.layout.admin')

@section('content')

       <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Ürün  <small>Listeleme</small></h3>
                   @if(Session::has('message'))
            <div class="alert alert-info">
                <p>{{ Session::get('message') }}</p>
            </div>
        @endif
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Tüm Ürünler</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <p>Simple table with project listing with progress and editing options</p>

                    <!-- start project list -->
                    <table class="table table-striped  hover">
                      <thead>
                        <tr>
                          <th style="width: 1%">#</th>
                          <th style="width: 20%">Ürün Adı</th>
                          <th> Foto</th>
                          <th>Ürün Özellikleri</th>
                          <th>Ürün Fiyatı</th>
                          <th>Durumu</th>
                          <th style="width: 20%">#İşlemler</th>
                        </tr>
                      </thead>
                      <tbody>

                        @forelse($products as $product)
                        <tr>
                          <td>#</td>
                          <td>
                            <a>{{$product->name}}</a>
                            <br />
                            <small>Created 01.01.2015</small>
                          </td>
                          <td>
                            <ul class="list-inline">
                            @foreach ($product->images as $image)
                              <li>
                                <img src="{{$image->image_path}}" style="max-width: 100px">
                              </li>
                            @endforeach
                            </ul>
                          </td>
                          <td>
                              {{$product->description}}                     
                          </td>
                          <td>
                            {{$product->price}}  
                          </td>
                          <td>
                            <div class="">
                            <label>
                              <input type="checkbox" class="js-switch" checked /> Aktif
                            </label>
                          </div>
                          </td>
                          <td>
                            <a href="{{route('product.edit',$product->id)}}" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> </a>
                            <a href="product/image/{{$product->id}}" class="btn btn-info btn-xs"><i class="fa fa-image"></i> </a>
                                                        
                             <button class="btn btn-xs btn-danger" id="ProductDelete" dataURL="{{route('product.destroy',$product->id)}}"  type="button"   ><i class="fa fa-trash-o"></i>Sil</button>
                            <button href="#" dataURL="{{route('product.destroy',$product->id)}}" onclick="confirmDelete()" >Delete record #23</button>
                            <a href="{{route('product.destroy',$product->id)}}" onclick="return confirm('Are you sure you want to delete this item?');">DeleteConirm</a>
                          </td>
                        </tr>
                         @empty

                        <h3>Listede Ürün Bulunamadı</h3>
                      
                        @endforelse
                      </tbody>
                    </table>
                    <!-- end project list -->

                  </div>
                </div>
              </div>
            </div>
          </div>  
          <a href="#" onclick="confirmDelete()">Ajax in this Delete will NOT work</a>
@endsection

@section('js')



<script>
    function confirmDelete() {
      var dataURL = $(this).attr("data-href");
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this imaginary file!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
    }, function (isConfirm) {
        if (!isConfirm) return;
        $.ajax({
             headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: dataURL,
            type: "DELETE",
            data: {
                id: 5
            },
            dataType: "html",
            success: function () {
                swal("Done!", "It was succesfully deleted!", "success");
            },
            error: function (xhr, ajaxOptions, thrownError) {
                swal("Error deleting!", "Please try again", "error");
            }
        });
    });
}

$(document).ready(function(){
    $("#ProductDelete").click(function(){
      var dataURL = $(this).attr("dataURL");
     swal({
      title: "Silmek İstediğinizden Emin Misiniz?",
      text: "Bu Ürün Tamamen Silinecek!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Evet , Sil Şunu!",
      closeOnConfirm: false
    },
    function(isConfirm){
      
           if (!isConfirm) return;
        $.ajax({

            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: dataURL,
            type: "DELETE",
           
            dataType: "html",
            success: function () {
                swal("Done!", "It was succesfully deleted!", "success");
                location.reload();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                swal("Error deleting!", "Please try again", "error");
            }
        });



    });
        });
      
      });

</script>

@endsection

@section('css')

@endsection


