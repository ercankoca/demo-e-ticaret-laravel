@extends('admin.layout.admin')

@section('content')

    <h3>Ürün Ekle</h3>
      @if(Session::has('message'))
            <div class="alert alert-info">
                <p>{{ Session::get('message') }}</p>
            </div>
        @endif
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            {!! Form::open(['route' => 'product.store', 'method' => 'POST', 'files' => true, 'data-parsley-validate'=>'']) !!}

            <div class="form-group">
                {{ Form::label('name', 'Ürün Adı') }}
                {{ Form::text('name', null, array('class' => 'form-control','required'=>'','minlength'=>'5')) }}
            </div>

            <div class="form-group">
                {{ Form::label('description', 'Ürün Açıklaması') }}
                {{ Form::text('description', null, array('class' => 'form-control')) }}
            </div>
            <div class="form-group">
                {{ Form::label('price', 'Ürün Fiyatı') }}
                {{ Form::text('price', null, array('class' => 'form-control')) }}
            </div>

           

            <div class="form-group">
                {{ Form::label('category_id', 'Kategoriler') }}
                {{ Form::select('category_id', $categories, null, ['class' => 'form-control','placeholder'=>'Kategori Seçiniz']) }}
            </div>

            <!--<div class="form-group">
                {{ Form::label('image', 'Ürün Resimi') }}
                {{ Form::file('image',array('class' => 'form-control')) }}
            </div>-->

             {{ Form::submit('Ekle', array('class' => 'btn btn-success')) }}
            {!! Form::close() !!}

        </div>
    </div>



@endsection