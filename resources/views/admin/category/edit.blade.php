@extends('admin.layout.admin')

@section('content')

    <h3>Kategoriyi Düzenle</h3>

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            {!! Form::model($category,['route' => ['category.update',$category->id], 'method' => 'PUT', 'files' => true]) !!}
            <div class="form-group">
                {{ Form::label('name', 'Kategori Adı') }}
                {{ Form::text('name', null, array('class' => 'form-control','required'=>'','minlength'=>'5')) }}
            </div>

   

             {{ Form::submit('Düzenle', array('class' => 'btn btn-success')) }}
            {!! Form::close() !!}


        </div>
    </div>



@endsection