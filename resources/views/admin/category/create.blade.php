@extends('admin.layout.admin')

@section('content')

	<h3>Ürün Ekle</h3>
      @if(Session::has('message'))
            <div class="alert alert-info">
                <p>{{ Session::get('message') }}</p>
            </div>
        @endif
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif



    {!! Form::open(['route' => 'category.store', 'method' => 'post' , 'id'=>'category' , 'class' => 'form-horizontal form-label-left' , 'data-parsley-validate'=>'']) !!}
    {{ Form::label('name','Kategori Adı',['class' => 'control-label col-md-3 col-sm-3 col-xs-12'])}}
 <div class="col-md-6 col-sm-6 col-xs-12">
    {{ Form::text('name','', ['class' => 'form-control col-md-7 col-xs-12'])}}
 </div>

    {{ Form::submit('Kaydet',['class' => 'btn btn-success']) }}
                        
    {!! Form::close() !!}
              

@endsection

@section('js')
<script src="{{ URL::to('') }}/admin/js/categories.js"></script>
@endsection
    
    
    