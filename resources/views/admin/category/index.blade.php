@extends('admin.layout.admin')

@section('content')

    <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Kategori  <small>Listeleme</small></h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Tüm Kategoriler</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <p>Simple table with project listing with progress and editing options</p>

                    <!-- start project list -->
                    <table class="table table-striped  table-hover">
                      <thead>
                        <tr>
                          <th style="width: 1%">#</th>
                          <th style="width: 20%">Kategori Adı</th>
                          <th>Durumu</th>
                          <th style="width: 20%">#Edit</th>
                        </tr>
                      </thead>
                      <tbody>

                        @forelse($categories as $category)
                        <tr>
                          <td>#</td>
                          <td>
                            <a>{{$category->name}}</a>
                            <br />
                            <small>{{$category->created_at}}</small>
                          </td>
                     
                          <td>
                            <input class = "toggle_check"
                                           data-onstyle="success"
                                           data-size = "mini"
                                           data-on="Aktif"
                                           data-off="Pasif"
                                           data-offstyle="danger"
                                           type="checkbox"
                                           data-toggle="toggle"
                                           dataID="<?php echo $category->id; ?>"
                                     <?php echo ($category->isActive == 1) ? "checked" : ""; ?>   
                                    />
                          </td>
                          <td>
                            <a href="{{route('category.edit',$category->id)}}" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Düzenle </a>
                            <form action="{{route('category.destroy',$category->id)}}" method="POST" style="float:right; padding-right: 35%">
                             {{csrf_field()}}
                             {{method_field('DELETE')}}
                             <button class="btn btn-xs btn-danger" id="CategoryDelete" dataURL="{{route('category.destroy',$category->id)}}"  type="button"   ><i class="fa fa-trash-o"></i>Sili</button>
                            <!--  <button data-toggle="tooltip"   data-placement="top" title="Sil"  dataURL="{{route('category.destroy',$category->id)}}" id="CategoryDelete"  class="btn btn-xs btn-danger" type="button" onclick="return confirm('{{$category->name}} Adlı Ürün Silinecek. Silmek istediğinizden emin misin')" ><i class="fa fa-trash-o"></i>Sil</button>

                            <a href="{{route('category.destroy',$category->id)}}" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Sil </a>
                           <input class="btn btn-danger btn-xs" type="submit"><i class="fa fa-trash-o"></i>-->
                                <!-- <a data-toggle="tooltip"   data-placement="top" title="Sil" class=" modal-basic btn btn-xl btn-danger"
                       href="{{route('category.destroy',$category->id)}}" onclick="return confirm('{{$category->name}} Adlı Ürün Silinecek. Silmek istediğinizden emin misin')"><i class="fa fa-trash"></i></a>-->
                
                
                           </form>

                           <a data-toggle="tooltip"   data-placement="top" title="Sil" class=" modal-basic btn btn-xl btn-danger"
                       href="{{route('category.destroy',$category->id)}}" onclick="return confirm('{{$category->name}} Adlı Ürün Silinecek. Silmek istediğinizden emin misin')"><i class="fa fa-trash"></i></a>
                          </td>
                        </tr>
                         @empty

                        <h3>Listede Kategori Bulunamadı</h3>
                      
                        @endforelse
                      </tbody>
                    </table>
                    <!-- end project list -->
                  
                </div>
              </div>
            </div>
          </div>     
       

@endsection

@section('js')

  <script>
    function confirmDelete() {
      var dataURL = $(this).attr("dataURL");
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this imaginary file!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
    }, function (isConfirm) {
        if (!isConfirm) return;
        $.ajax({
             headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: dataURL,
            type: "DELETE",
            data: {
                id: 5
            },
            dataType: "html",
            success: function () {
                swal("Done!", "It was succesfully deleted!", "success");
            },
            error: function (xhr, ajaxOptions, thrownError) {
                swal("Error deleting!", "Please try again", "error");
            }
        });
    });
}

$(document).ready(function(){
    $("#CategoryDelete").click(function(){
      var dataURL = $(this).attr("dataURL");
     swal({
      title: "Silmek İstediğinizden Emin Misiniz?",
      text: "Bu Ürün Tamamen Silinecek!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Evet , Sil Şunu!",
      closeOnConfirm: false
    },
    function(isConfirm){
      
           if (!isConfirm) return;
        $.ajax({

            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: dataURL,
            type: "DELETE",
           
            dataType: "html",
            success: function () {
                swal("Done!", "It was succesfully deleted!", "success");
                location.reload();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                swal("Error deleting!", "Please try again", "error");
            }
        });



    });
        });
      
      });




</script>

@endsection

<link rel="stylesheet" type="text/css" href="/admin/css/bootstrap-toggle.min.css">
@section('css')
    
    
    