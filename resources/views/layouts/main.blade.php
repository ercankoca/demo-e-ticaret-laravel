<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Depoin - Yapı Malzemeleri</title>	

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

		<!-- Basic -->
		
			
		<meta name="keywords" content="HTML5 Template" />
		<meta name="description" content="Porto - Responsive HTML5 Template">
		<meta name="author" content="okler.net">

		   <!-- Scripts -->
	    <script src="{{ asset('js/app.js') }}" defer></script>

	    <!-- Fonts -->
	    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
	    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

	    <!-- Styles -->
	    
			

		<!-- Web Fonts  -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<base href="{{ URL::to('') }}">

		<link rel="stylesheet" href="/frontend/vendor/bootstrap/bootstrap.css">
		<link rel="stylesheet" href="/frontend/vendor/fontawesome/css/font-awesome.css">
		<link rel="stylesheet" href="/frontend/vendor/owlcarousel/owl.carousel.min.css" media="screen">
		<link rel="stylesheet" href="/frontend/vendor/owlcarousel/owl.theme.default.min.css" media="screen">
		<link rel="stylesheet" href="/frontend/vendor/magnific-popup/magnific-popup.css" media="screen">

		<!-- Theme CSS -->
		<link rel="stylesheet" href="/frontend/css/theme.css">
		<link rel="stylesheet" href="/frontend/css/theme-elements.css">
		<link rel="stylesheet" href="/frontend/css/theme-blog.css">
		<link rel="stylesheet" href="/frontend/css/theme-shop.css">
		<link rel="stylesheet" href="/frontend/css/theme-animate.css">
		<link rel="stylesheet" href="/frontend/css/base.css">
	
		<link rel="stylesheet" href="/css/style.css">
		
		
	

		<!-- Current Page CSS -->
		<link rel="stylesheet" href="/frontend/vendor/rs-plugin/css/settings.css" media="screen">
		<link rel="stylesheet" href="/frontend/vendor/circle-flip-slideshow/css/component.css" media="screen">

		<!-- Skin CSS -->
		<link rel="stylesheet" href="/frontend/css/skins/default.css">

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="/frontend/css/custom.css">

		<!-- Head Libs -->
		<script src="/frontend/vendor/modernizr/modernizr.js"></script>

		<!--[if IE]>
			<link rel="stylesheet" href="css/ie.css">
		<![endif]-->

		<!--[if lte IE 8]>
			<script src="vendor/respond/respond.js"></script>
			<script src="vendor/excanvas/excanvas.js"></script>
		<![endif]-->
		

	</head>
		<body>
		<div class="body">
			<header id="header">
				<div class="container">
					<div class="logo">
						<a href="/">
							<img alt="Porto" width="111" height="54" data-sticky-width="82" data-sticky-height="40" src="/frontend/img/logo.png">
						</a>

					</div>

					<div class="search">
						<form id="searchForm" action="page-search-results.html" method="get">
							<div class="input-group">
								<input type="text" class="form-control search" name="q" id="q" placeholder="Search..." required>
								<span class="input-group-btn">
									<button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
								</span>
							</div>
						</form>
					</div>
					<nav>
						<ul class="nav nav-pills nav-top">
							<li>
								<a href="about-us.html"><i class="fa fa-angle-right"></i>About Us</a>
							</li>
							<li>
								<a href="contact-us.html"><i class="fa fa-angle-right"></i>Contact Us</a>
							</li>
							<li class="phone">
								<span><i class="fa fa-phone"></i>(123) 456-7890</span>
							</li>
							     <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Giriş Yap') }}</a>
                            </li>
                            <!--<li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>-->
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
						</ul>

					</nav>
						<nav>
					 <ul class="nav nav-pills nav-top">
                   
                    </ul>

					</nav>
					<button class="btn btn-responsive-nav btn-inverse" data-toggle="collapse" data-target=".nav-main-collapse">
						<i class="fa fa-bars"></i>
					</button>

				</div><br>
				<div class="navbar-collapse nav-main-collapse collapse">
					<div class="container">
						<ul class="social-icons" style="margin-bottom: 10px">
							<li class="facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook">Facebook</a></li>
							<li class="twitter"><a href="http://www.twitter.com/" target="_blank" title="Twitter">Twitter</a></li>
							<li class="linkedin"><a href="http://www.linkedin.com/" target="_blank" title="Linkedin">Linkedin</a></li>
						</ul>
						<nav class="nav-main mega-menu">
							<ul class="nav nav-pills nav-main" id="mainMenu" style="background-color: black">
							<li class="dropdown mega-menu-item mega-menu-fullwidth ">
									<a class="dropdown-toggle" href="#" >
										<span >Mobİlya</span>
										
									</a>
									<ul class="dropdown-menu">
										<li>
											<div class="mega-menu-content">
												<div class="row">
													<div class="col-md-3">
														<ul class="sub-menu">
															<li>
																
																<ul class="sub-menu">
																	<li><a class="singlemenu" href="/mobilya/gardiroplar">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/gardiroplar.jpg" style="font-weight: bold">
	                                      								Gardıroplar
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/bilgisayar_ve_calisma_masasi.jpg" style="font-weight: bold">
	                                      								Bilgisayar ve Çalışma Masası
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/koltuk_ve_kanepeler.jpg" style="font-weight: bold">
	                                      								Koltuk ve Kanepeler
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/kitaplik.jpg" style="font-weight: bold">
	                                      								Kitaplık
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/tv_unitesi_ve_sehpasi.jpg" style="font-weight: bold">
	                                      								TV Ünitesi ve Sehpası
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/cok_amacli_dolap.jpg" style="font-weight: bold">
	                                      								Çok Amaçlı Dolap
																	</a></li>
																</ul>
															</li>
														</ul>
													</div>
													<div class="col-md-3">
														<ul class="sub-menu">
															<li>
															
																<ul class="sub-menu">
															<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Genç Odası Takımları" title="Genç Odası Takımları" class="lazy mddMenuImages" src="/frontend/img/categories/genc_odasi_takimlari.jpg" style="font-weight: bold">
	                                      								Genç Odası Takımları
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Yatak Odası Takımları" title="Yatak Odası Takımları" class="lazy mddMenuImages" src="/frontend/img/categories/yatak_odasi_takimlari.jpg" style="font-weight: bold">
	                                      								Yatak Odası Takımları
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Mutfak Masaları" title="Mutfak Masaları" class="lazy mddMenuImages" src="/frontend/img/categories/mutfak_masalari.jpg" style="font-weight: bold">
	                                      								Mutfak Masaları
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Dekoratif Raflar" title="Dekoratif Raflar" class="lazy mddMenuImages" src="/frontend/img/categories/raf.jpg" style="font-weight: bold">
	                                      								Dekoratif Raflar
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Baza ve Karyolalar" title="Baza ve Karyolalar" class="lazy mddMenuImages" src="/frontend/img/categories/karyola.jpg" style="font-weight: bold">
	                                      								Baza ve Karyolalar
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Şifonyer" title="Şifonyer" class="lazy mddMenuImages" src="/frontend/img/categories/sifonyer.jpg" style="font-weight: bold">
	                                      								Şifonyer
																	</a></li>
																</ul>
															</li>
														</ul>
													</div>
													<div class="col-md-3">
														<ul class="sub-menu">
															<li>
																
																<ul class="sub-menu">
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Portmanto ve Askılık" title="Portmanto ve Askılık" class="lazy mddMenuImages" src="/frontend/img/categories/portmanto_ve_askilik.jpg" style="font-weight: bold">
	                                      								Portmanto ve Askılık
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Ofis Koltuğu ve Sandalyeler" title="Ofis Koltuğu ve Sandalyeler" class="lazy mddMenuImages" src="/frontend/img/categories/calisma_sandalyeleri.jpg" style="font-weight: bold">
	                                      								Ofis Koltuğu ve Sandalyeler
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Takı Dolapları" title="Takı Dolapları" class="lazy mddMenuImages" src="/frontend/img/categories/taki_dolaplari.jpg" style="font-weight: bold">
	                                      								Takı Dolapları
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Komodin" title="Komodin" class="lazy mddMenuImages" src="/frontend/img/categories/komodin.jpg" style="font-weight: bold">
	                                      								Komodin
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Mobilya Aksesuarları" title="Mobilya Aksesuarları" class="lazy mddMenuImages" src="/frontend/img/categories/mobilya_aksesuarlari.jpg" style="font-weight: bold">
	                                      								Mobilya Aksesuarları
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Sandalyeler" title="Sandalyeler" class="lazy mddMenuImages" src="/frontend/img/categories/sandalyeler.jpg" style="font-weight: bold">
	                                      								Sandalyeler
																	</a></li>
																</ul>
															</li>
														</ul>
													</div>
													<div class="col-md-3">
														<ul class="sub-menu">
															<li>
																
																<ul class="sub-menu">
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Yataklar" title="Yataklar" class="lazy mddMenuImages" src="/frontend/img/categories/yatak.jpg" style="font-weight: bold">
	                                      								Yataklar
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Sehpalar" title="Sehpalar" class="lazy mddMenuImages" src="/frontend/img/categories/zigon_sehpa.jpg" style="font-weight: bold">
	                                      								Sehpalar
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Ayakkabılık" title="Ayakkabılık" class="lazy mddMenuImages" src="/frontend/img/categories/ayakkabilik.jpg" style="font-weight: bold">
	                                      								Ayakkabılık
																	</a></li>
																
																</ul>
															</li>
														</ul>
													</div>
												</div>
											</div>
										</li>
									</ul>
								</li>
							<li class="dropdown mega-menu-item mega-menu-fullwidth">
									<a class="dropdown-toggle" href="#">
										<span style="width:55px; max-width: 40%;">Bahçe ve Balkon</span>
										
									</a>
									<ul class="dropdown-menu">
										<li>
											<div class="mega-menu-content">
												<div class="row">
													<div class="col-md-3">
														<ul class="sub-menu">
															<li>
																
																<ul class="sub-menu">
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/gardiroplar.jpg" style="font-weight: bold">
	                                      								Gardıroplar
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/bilgisayar_ve_calisma_masasi.jpg" style="font-weight: bold">
	                                      								Bilgisayar ve Çalışma Masası
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/koltuk_ve_kanepeler.jpg" style="font-weight: bold">
	                                      								Koltuk ve Kanepeler
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/kitaplik.jpg" style="font-weight: bold">
	                                      								Kitaplık
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/tv_unitesi_ve_sehpasi.jpg" style="font-weight: bold">
	                                      								TV Ünitesi ve Sehpası
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/cok_amacli_dolap.jpg" style="font-weight: bold">
	                                      								Çok Amaçlı Dolap
																	</a></li>
																</ul>
															</li>
														</ul>
													</div>
													<div class="col-md-3">
														<ul class="sub-menu">
															<li>
															
																<ul class="sub-menu">
															<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Genç Odası Takımları" title="Genç Odası Takımları" class="lazy mddMenuImages" src="/frontend/img/categories/genc_odasi_takimlari.jpg" style="font-weight: bold">
	                                      								Genç Odası Takımları
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Yatak Odası Takımları" title="Yatak Odası Takımları" class="lazy mddMenuImages" src="/frontend/img/categories/yatak_odasi_takimlari.jpg" style="font-weight: bold">
	                                      								Yatak Odası Takımları
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Mutfak Masaları" title="Mutfak Masaları" class="lazy mddMenuImages" src="/frontend/img/categories/mutfak_masalari.jpg" style="font-weight: bold">
	                                      								Mutfak Masaları
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Dekoratif Raflar" title="Dekoratif Raflar" class="lazy mddMenuImages" src="/frontend/img/categories/raf.jpg" style="font-weight: bold">
	                                      								Dekoratif Raflar
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Baza ve Karyolalar" title="Baza ve Karyolalar" class="lazy mddMenuImages" src="/frontend/img/categories/karyola.jpg" style="font-weight: bold">
	                                      								Baza ve Karyolalar
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Şifonyer" title="Şifonyer" class="lazy mddMenuImages" src="/frontend/img/categories/sifonyer.jpg" style="font-weight: bold">
	                                      								Şifonyer
																	</a></li>
																</ul>
															</li>
														</ul>
													</div>
													<div class="col-md-3">
														<ul class="sub-menu">
															<li>
																
																<ul class="sub-menu">
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Portmanto ve Askılık" title="Portmanto ve Askılık" class="lazy mddMenuImages" src="/frontend/img/categories/portmanto_ve_askilik.jpg" style="font-weight: bold">
	                                      								Portmanto ve Askılık
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Ofis Koltuğu ve Sandalyeler" title="Ofis Koltuğu ve Sandalyeler" class="lazy mddMenuImages" src="/frontend/img/categories/calisma_sandalyeleri.jpg" style="font-weight: bold">
	                                      								Ofis Koltuğu ve Sandalyeler
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Takı Dolapları" title="Takı Dolapları" class="lazy mddMenuImages" src="/frontend/img/categories/taki_dolaplari.jpg" style="font-weight: bold">
	                                      								Takı Dolapları
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Komodin" title="Komodin" class="lazy mddMenuImages" src="/frontend/img/categories/komodin.jpg" style="font-weight: bold">
	                                      								Komodin
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Mobilya Aksesuarları" title="Mobilya Aksesuarları" class="lazy mddMenuImages" src="/frontend/img/categories/mobilya_aksesuarlari.jpg" style="font-weight: bold">
	                                      								Mobilya Aksesuarları
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Sandalyeler" title="Sandalyeler" class="lazy mddMenuImages" src="/frontend/img/categories/sandalyeler.jpg" style="font-weight: bold">
	                                      								Sandalyeler
																	</a></li>
																</ul>
															</li>
														</ul>
													</div>
													<div class="col-md-3">
														<ul class="sub-menu">
															<li>
																
																<ul class="sub-menu">
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Yataklar" title="Yataklar" class="lazy mddMenuImages" src="/frontend/img/categories/yatak.jpg" style="font-weight: bold">
	                                      								Yataklar
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Sehpalar" title="Sehpalar" class="lazy mddMenuImages" src="/frontend/img/categories/zigon_sehpa.jpg" style="font-weight: bold">
	                                      								Sehpalar
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Ayakkabılık" title="Ayakkabılık" class="lazy mddMenuImages" src="/frontend/img/categories/ayakkabilik.jpg" style="font-weight: bold">
	                                      								Ayakkabılık
																	</a></li>
																
																</ul>
															</li>
														</ul>
													</div>
												</div>
											</div>
										</li>
									</ul>
								</li>
								
								<li class="dropdown mega-menu-item mega-menu-fullwidth">
									<a class="dropdown-toggle" href="#">
										<span style="width:60px; max-width: 40%;">Isıtma Ve Soğutma </span>	
										
									</a>
									<ul class="dropdown-menu" >
										<li>
											<div class="mega-menu-content">
												<div class="row">
													<div class="col-md-3">
														<ul class="sub-menu">
															<li>
																
																<ul class="sub-menu">
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/gardiroplar.jpg" style="font-weight: bold">
	                                      								Gardıroplar
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/bilgisayar_ve_calisma_masasi.jpg" style="font-weight: bold">
	                                      								Bilgisayar ve Çalışma Masası
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/koltuk_ve_kanepeler.jpg" style="font-weight: bold">
	                                      								Koltuk ve Kanepeler
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/kitaplik.jpg" style="font-weight: bold">
	                                      								Kitaplık
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/tv_unitesi_ve_sehpasi.jpg" style="font-weight: bold">
	                                      								TV Ünitesi ve Sehpası
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/cok_amacli_dolap.jpg" style="font-weight: bold">
	                                      								Çok Amaçlı Dolap
																	</a></li>
																</ul>
															</li>
														</ul>
													</div>
													<div class="col-md-3">
														<ul class="sub-menu">
															<li>
															
																<ul class="sub-menu">
															<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Genç Odası Takımları" title="Genç Odası Takımları" class="lazy mddMenuImages" src="/frontend/img/categories/genc_odasi_takimlari.jpg" style="font-weight: bold">
	                                      								Genç Odası Takımları
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Yatak Odası Takımları" title="Yatak Odası Takımları" class="lazy mddMenuImages" src="/frontend/img/categories/yatak_odasi_takimlari.jpg" style="font-weight: bold">
	                                      								Yatak Odası Takımları
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Mutfak Masaları" title="Mutfak Masaları" class="lazy mddMenuImages" src="/frontend/img/categories/mutfak_masalari.jpg" style="font-weight: bold">
	                                      								Mutfak Masaları
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Dekoratif Raflar" title="Dekoratif Raflar" class="lazy mddMenuImages" src="/frontend/img/categories/raf.jpg" style="font-weight: bold">
	                                      								Dekoratif Raflar
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Baza ve Karyolalar" title="Baza ve Karyolalar" class="lazy mddMenuImages" src="/frontend/img/categories/karyola.jpg" style="font-weight: bold">
	                                      								Baza ve Karyolalar
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Şifonyer" title="Şifonyer" class="lazy mddMenuImages" src="/frontend/img/categories/sifonyer.jpg" style="font-weight: bold">
	                                      								Şifonyer
																	</a></li>
																</ul>
															</li>
														</ul>
													</div>
													<div class="col-md-3">
														<ul class="sub-menu">
															<li>
																
																<ul class="sub-menu">
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Portmanto ve Askılık" title="Portmanto ve Askılık" class="lazy mddMenuImages" src="/frontend/img/categories/portmanto_ve_askilik.jpg" style="font-weight: bold">
	                                      								Portmanto ve Askılık
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Ofis Koltuğu ve Sandalyeler" title="Ofis Koltuğu ve Sandalyeler" class="lazy mddMenuImages" src="/frontend/img/categories/calisma_sandalyeleri.jpg" style="font-weight: bold">
	                                      								Ofis Koltuğu ve Sandalyeler
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Takı Dolapları" title="Takı Dolapları" class="lazy mddMenuImages" src="/frontend/img/categories/taki_dolaplari.jpg" style="font-weight: bold">
	                                      								Takı Dolapları
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Komodin" title="Komodin" class="lazy mddMenuImages" src="/frontend/img/categories/komodin.jpg" style="font-weight: bold">
	                                      								Komodin
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Mobilya Aksesuarları" title="Mobilya Aksesuarları" class="lazy mddMenuImages" src="/frontend/img/categories/mobilya_aksesuarlari.jpg" style="font-weight: bold">
	                                      								Mobilya Aksesuarları
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Sandalyeler" title="Sandalyeler" class="lazy mddMenuImages" src="/frontend/img/categories/sandalyeler.jpg" style="font-weight: bold">
	                                      								Sandalyeler
																	</a></li>
																</ul>
															</li>
														</ul>
													</div>
													<div class="col-md-3">
														<ul class="sub-menu">
															<li>
																
																<ul class="sub-menu">
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Yataklar" title="Yataklar" class="lazy mddMenuImages" src="/frontend/img/categories/yatak.jpg" style="font-weight: bold">
	                                      								Yataklar
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Sehpalar" title="Sehpalar" class="lazy mddMenuImages" src="/frontend/img/categories/zigon_sehpa.jpg" style="font-weight: bold">
	                                      								Sehpalar
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Ayakkabılık" title="Ayakkabılık" class="lazy mddMenuImages" src="/frontend/img/categories/ayakkabilik.jpg" style="font-weight: bold">
	                                      								Ayakkabılık
																	</a></li>
																
																</ul>
															</li>
														</ul>
													</div>
												</div>
											</div>
										</li>
									</ul>
								</li>
								
								<li class="dropdown mega-menu-item mega-menu-fullwidth">
									<a class="dropdown-toggle" href="#">
										<span>Banyo</span>
										
									</a>
									<ul class="dropdown-menu">
										<li>
											<div class="mega-menu-content">
												<div class="row">
													<div class="col-md-3">
														<ul class="sub-menu">
															<li>
																
																<ul class="sub-menu">
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/gardiroplar.jpg" style="font-weight: bold">
	                                      								Gardıroplar
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/bilgisayar_ve_calisma_masasi.jpg" style="font-weight: bold">
	                                      								Bilgisayar ve Çalışma Masası
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/koltuk_ve_kanepeler.jpg" style="font-weight: bold">
	                                      								Koltuk ve Kanepeler
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/kitaplik.jpg" style="font-weight: bold">
	                                      								Kitaplık
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/tv_unitesi_ve_sehpasi.jpg" style="font-weight: bold">
	                                      								TV Ünitesi ve Sehpası
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/cok_amacli_dolap.jpg" style="font-weight: bold">
	                                      								Çok Amaçlı Dolap
																	</a></li>
																</ul>
															</li>
														</ul>
													</div>
													<div class="col-md-3">
														<ul class="sub-menu">
															<li>
															
																<ul class="sub-menu">
															<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Genç Odası Takımları" title="Genç Odası Takımları" class="lazy mddMenuImages" src="/frontend/img/categories/genc_odasi_takimlari.jpg" style="font-weight: bold">
	                                      								Genç Odası Takımları
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Yatak Odası Takımları" title="Yatak Odası Takımları" class="lazy mddMenuImages" src="/frontend/img/categories/yatak_odasi_takimlari.jpg" style="font-weight: bold">
	                                      								Yatak Odası Takımları
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Mutfak Masaları" title="Mutfak Masaları" class="lazy mddMenuImages" src="/frontend/img/categories/mutfak_masalari.jpg" style="font-weight: bold">
	                                      								Mutfak Masaları
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Dekoratif Raflar" title="Dekoratif Raflar" class="lazy mddMenuImages" src="/frontend/img/categories/raf.jpg" style="font-weight: bold">
	                                      								Dekoratif Raflar
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Baza ve Karyolalar" title="Baza ve Karyolalar" class="lazy mddMenuImages" src="/frontend/img/categories/karyola.jpg" style="font-weight: bold">
	                                      								Baza ve Karyolalar
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Şifonyer" title="Şifonyer" class="lazy mddMenuImages" src="/frontend/img/categories/sifonyer.jpg" style="font-weight: bold">
	                                      								Şifonyer
																	</a></li>
																</ul>
															</li>
														</ul>
													</div>
													<div class="col-md-3">
														<ul class="sub-menu">
															<li>
																
																<ul class="sub-menu">
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Portmanto ve Askılık" title="Portmanto ve Askılık" class="lazy mddMenuImages" src="/frontend/img/categories/portmanto_ve_askilik.jpg" style="font-weight: bold">
	                                      								Portmanto ve Askılık
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Ofis Koltuğu ve Sandalyeler" title="Ofis Koltuğu ve Sandalyeler" class="lazy mddMenuImages" src="/frontend/img/categories/calisma_sandalyeleri.jpg" style="font-weight: bold">
	                                      								Ofis Koltuğu ve Sandalyeler
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Takı Dolapları" title="Takı Dolapları" class="lazy mddMenuImages" src="/frontend/img/categories/taki_dolaplari.jpg" style="font-weight: bold">
	                                      								Takı Dolapları
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Komodin" title="Komodin" class="lazy mddMenuImages" src="/frontend/img/categories/komodin.jpg" style="font-weight: bold">
	                                      								Komodin
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Mobilya Aksesuarları" title="Mobilya Aksesuarları" class="lazy mddMenuImages" src="/frontend/img/categories/mobilya_aksesuarlari.jpg" style="font-weight: bold">
	                                      								Mobilya Aksesuarları
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Sandalyeler" title="Sandalyeler" class="lazy mddMenuImages" src="/frontend/img/categories/sandalyeler.jpg" style="font-weight: bold">
	                                      								Sandalyeler
																	</a></li>
																</ul>
															</li>
														</ul>
													</div>
													<div class="col-md-3">
														<ul class="sub-menu">
															<li>
																
																<ul class="sub-menu">
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Yataklar" title="Yataklar" class="lazy mddMenuImages" src="/frontend/img/categories/yatak.jpg" style="font-weight: bold">
	                                      								Yataklar
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Sehpalar" title="Sehpalar" class="lazy mddMenuImages" src="/frontend/img/categories/zigon_sehpa.jpg" style="font-weight: bold">
	                                      								Sehpalar
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Ayakkabılık" title="Ayakkabılık" class="lazy mddMenuImages" src="/frontend/img/categories/ayakkabilik.jpg" style="font-weight: bold">
	                                      								Ayakkabılık
																	</a></li>
																
																</ul>
															</li>
														</ul>
													</div>
												</div>
											</div>
										</li>
									</ul>
								</li>
								<li class="dropdown mega-menu-item mega-menu-fullwidth">
									<a class="dropdown-toggle" href="#">
										<span>Mutfak</span>
										
									</a>
									<ul class="dropdown-menu">
										<li>
											<div class="mega-menu-content">
												<div class="row">
													<div class="col-md-3">
														<ul class="sub-menu">
															<li>
																
																<ul class="sub-menu">
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/gardiroplar.jpg" style="font-weight: bold">
	                                      								Gardıroplar
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/bilgisayar_ve_calisma_masasi.jpg" style="font-weight: bold">
	                                      								Bilgisayar ve Çalışma Masası
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/koltuk_ve_kanepeler.jpg" style="font-weight: bold">
	                                      								Koltuk ve Kanepeler
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/kitaplik.jpg" style="font-weight: bold">
	                                      								Kitaplık
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/tv_unitesi_ve_sehpasi.jpg" style="font-weight: bold">
	                                      								TV Ünitesi ve Sehpası
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/cok_amacli_dolap.jpg" style="font-weight: bold">
	                                      								Çok Amaçlı Dolap
																	</a></li>
																</ul>
															</li>
														</ul>
													</div>
													<div class="col-md-3">
														<ul class="sub-menu">
															<li>
															
																<ul class="sub-menu">
															<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Genç Odası Takımları" title="Genç Odası Takımları" class="lazy mddMenuImages" src="/frontend/img/categories/genc_odasi_takimlari.jpg" style="font-weight: bold">
	                                      								Genç Odası Takımları
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Yatak Odası Takımları" title="Yatak Odası Takımları" class="lazy mddMenuImages" src="/frontend/img/categories/yatak_odasi_takimlari.jpg" style="font-weight: bold">
	                                      								Yatak Odası Takımları
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Mutfak Masaları" title="Mutfak Masaları" class="lazy mddMenuImages" src="/frontend/img/categories/mutfak_masalari.jpg" style="font-weight: bold">
	                                      								Mutfak Masaları
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Dekoratif Raflar" title="Dekoratif Raflar" class="lazy mddMenuImages" src="/frontend/img/categories/raf.jpg" style="font-weight: bold">
	                                      								Dekoratif Raflar
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Baza ve Karyolalar" title="Baza ve Karyolalar" class="lazy mddMenuImages" src="/frontend/img/categories/karyola.jpg" style="font-weight: bold">
	                                      								Baza ve Karyolalar
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Şifonyer" title="Şifonyer" class="lazy mddMenuImages" src="/frontend/img/categories/sifonyer.jpg" style="font-weight: bold">
	                                      								Şifonyer
																	</a></li>
																</ul>
															</li>
														</ul>
													</div>
													<div class="col-md-3">
														<ul class="sub-menu">
															<li>
																
																<ul class="sub-menu">
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Portmanto ve Askılık" title="Portmanto ve Askılık" class="lazy mddMenuImages" src="/frontend/img/categories/portmanto_ve_askilik.jpg" style="font-weight: bold">
	                                      								Portmanto ve Askılık
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Ofis Koltuğu ve Sandalyeler" title="Ofis Koltuğu ve Sandalyeler" class="lazy mddMenuImages" src="/frontend/img/categories/calisma_sandalyeleri.jpg" style="font-weight: bold">
	                                      								Ofis Koltuğu ve Sandalyeler
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Takı Dolapları" title="Takı Dolapları" class="lazy mddMenuImages" src="/frontend/img/categories/taki_dolaplari.jpg" style="font-weight: bold">
	                                      								Takı Dolapları
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Komodin" title="Komodin" class="lazy mddMenuImages" src="/frontend/img/categories/komodin.jpg" style="font-weight: bold">
	                                      								Komodin
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Mobilya Aksesuarları" title="Mobilya Aksesuarları" class="lazy mddMenuImages" src="/frontend/img/categories/mobilya_aksesuarlari.jpg" style="font-weight: bold">
	                                      								Mobilya Aksesuarları
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Sandalyeler" title="Sandalyeler" class="lazy mddMenuImages" src="/frontend/img/categories/sandalyeler.jpg" style="font-weight: bold">
	                                      								Sandalyeler
																	</a></li>
																</ul>
															</li>
														</ul>
													</div>
													<div class="col-md-3">
														<ul class="sub-menu">
															<li>
																
																<ul class="sub-menu">
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Yataklar" title="Yataklar" class="lazy mddMenuImages" src="/frontend/img/categories/yatak.jpg" style="font-weight: bold">
	                                      								Yataklar
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Sehpalar" title="Sehpalar" class="lazy mddMenuImages" src="/frontend/img/categories/zigon_sehpa.jpg" style="font-weight: bold">
	                                      								Sehpalar
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Ayakkabılık" title="Ayakkabılık" class="lazy mddMenuImages" src="/frontend/img/categories/ayakkabilik.jpg" style="font-weight: bold">
	                                      								Ayakkabılık
																	</a></li>
																
																</ul>
															</li>
														</ul>
													</div>
												</div>
											</div>
										</li>
									</ul>
								</li>
								<li class="dropdown mega-menu-item mega-menu-fullwidth">
									<a class="dropdown-toggle" href="#">
										<span style="width:100px; max-width: 40%;">Dekorasyon ve Ev Gereçleri</span>
										
									</a>
									<ul class="dropdown-menu">
										<li>
											<div class="mega-menu-content">
												<div class="row">
													<div class="col-md-3">
														<ul class="sub-menu">
															<li>
																
																<ul class="sub-menu">
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/gardiroplar.jpg" style="font-weight: bold">
	                                      								Gardıroplar
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/bilgisayar_ve_calisma_masasi.jpg" style="font-weight: bold">
	                                      								Bilgisayar ve Çalışma Masası
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/koltuk_ve_kanepeler.jpg" style="font-weight: bold">
	                                      								Koltuk ve Kanepeler
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/kitaplik.jpg" style="font-weight: bold">
	                                      								Kitaplık
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/tv_unitesi_ve_sehpasi.jpg" style="font-weight: bold">
	                                      								TV Ünitesi ve Sehpası
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/cok_amacli_dolap.jpg" style="font-weight: bold">
	                                      								Çok Amaçlı Dolap
																	</a></li>
																</ul>
															</li>
														</ul>
													</div>
													<div class="col-md-3">
														<ul class="sub-menu">
															<li>
															
																<ul class="sub-menu">
															<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Genç Odası Takımları" title="Genç Odası Takımları" class="lazy mddMenuImages" src="/frontend/img/categories/genc_odasi_takimlari.jpg" style="font-weight: bold">
	                                      								Genç Odası Takımları
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Yatak Odası Takımları" title="Yatak Odası Takımları" class="lazy mddMenuImages" src="/frontend/img/categories/yatak_odasi_takimlari.jpg" style="font-weight: bold">
	                                      								Yatak Odası Takımları
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Mutfak Masaları" title="Mutfak Masaları" class="lazy mddMenuImages" src="/frontend/img/categories/mutfak_masalari.jpg" style="font-weight: bold">
	                                      								Mutfak Masaları
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Dekoratif Raflar" title="Dekoratif Raflar" class="lazy mddMenuImages" src="/frontend/img/categories/raf.jpg" style="font-weight: bold">
	                                      								Dekoratif Raflar
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Baza ve Karyolalar" title="Baza ve Karyolalar" class="lazy mddMenuImages" src="/frontend/img/categories/karyola.jpg" style="font-weight: bold">
	                                      								Baza ve Karyolalar
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Şifonyer" title="Şifonyer" class="lazy mddMenuImages" src="/frontend/img/categories/sifonyer.jpg" style="font-weight: bold">
	                                      								Şifonyer
																	</a></li>
																</ul>
															</li>
														</ul>
													</div>
													<div class="col-md-3">
														<ul class="sub-menu">
															<li>
																
																<ul class="sub-menu">
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Portmanto ve Askılık" title="Portmanto ve Askılık" class="lazy mddMenuImages" src="/frontend/img/categories/portmanto_ve_askilik.jpg" style="font-weight: bold">
	                                      								Portmanto ve Askılık
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Ofis Koltuğu ve Sandalyeler" title="Ofis Koltuğu ve Sandalyeler" class="lazy mddMenuImages" src="/frontend/img/categories/calisma_sandalyeleri.jpg" style="font-weight: bold">
	                                      								Ofis Koltuğu ve Sandalyeler
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Takı Dolapları" title="Takı Dolapları" class="lazy mddMenuImages" src="/frontend/img/categories/taki_dolaplari.jpg" style="font-weight: bold">
	                                      								Takı Dolapları
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Komodin" title="Komodin" class="lazy mddMenuImages" src="/frontend/img/categories/komodin.jpg" style="font-weight: bold">
	                                      								Komodin
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Mobilya Aksesuarları" title="Mobilya Aksesuarları" class="lazy mddMenuImages" src="/frontend/img/categories/mobilya_aksesuarlari.jpg" style="font-weight: bold">
	                                      								Mobilya Aksesuarları
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Sandalyeler" title="Sandalyeler" class="lazy mddMenuImages" src="/frontend/img/categories/sandalyeler.jpg" style="font-weight: bold">
	                                      								Sandalyeler
																	</a></li>
																</ul>
															</li>
														</ul>
													</div>
													<div class="col-md-3">
														<ul class="sub-menu">
															<li>
																
																<ul class="sub-menu">
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Yataklar" title="Yataklar" class="lazy mddMenuImages" src="/frontend/img/categories/yatak.jpg" style="font-weight: bold">
	                                      								Yataklar
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Sehpalar" title="Sehpalar" class="lazy mddMenuImages" src="/frontend/img/categories/zigon_sehpa.jpg" style="font-weight: bold">
	                                      								Sehpalar
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Ayakkabılık" title="Ayakkabılık" class="lazy mddMenuImages" src="/frontend/img/categories/ayakkabilik.jpg" style="font-weight: bold">
	                                      								Ayakkabılık
																	</a></li>
																
																</ul>
															</li>
														</ul>
													</div>
												</div>
											</div>
										</li>
									</ul>
								</li>

								<li class="dropdown mega-menu-item mega-menu-fullwidth">
									<a class="dropdown-toggle" href="#">
										<span style="width:100px; max-width: 40%;">Aydınlatma ve Elektrik</span>
										
									</a>
									<ul class="dropdown-menu">
										<li>
											<div class="mega-menu-content">
												<div class="row">
													<div class="col-md-3">
														<ul class="sub-menu">
															<li>
																
																<ul class="sub-menu">
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/gardiroplar.jpg" style="font-weight: bold">
	                                      								Gardıroplar
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/bilgisayar_ve_calisma_masasi.jpg" style="font-weight: bold">
	                                      								Bilgisayar ve Çalışma Masası
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/koltuk_ve_kanepeler.jpg" style="font-weight: bold">
	                                      								Koltuk ve Kanepeler
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/kitaplik.jpg" style="font-weight: bold">
	                                      								Kitaplık
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/tv_unitesi_ve_sehpasi.jpg" style="font-weight: bold">
	                                      								TV Ünitesi ve Sehpası
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/cok_amacli_dolap.jpg" style="font-weight: bold">
	                                      								Çok Amaçlı Dolap
																	</a></li>
																</ul>
															</li>
														</ul>
													</div>
													<div class="col-md-3">
														<ul class="sub-menu">
															<li>
															
																<ul class="sub-menu">
															<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Genç Odası Takımları" title="Genç Odası Takımları" class="lazy mddMenuImages" src="/frontend/img/categories/genc_odasi_takimlari.jpg" style="font-weight: bold">
	                                      								Genç Odası Takımları
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Yatak Odası Takımları" title="Yatak Odası Takımları" class="lazy mddMenuImages" src="/frontend/img/categories/yatak_odasi_takimlari.jpg" style="font-weight: bold">
	                                      								Yatak Odası Takımları
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Mutfak Masaları" title="Mutfak Masaları" class="lazy mddMenuImages" src="/frontend/img/categories/mutfak_masalari.jpg" style="font-weight: bold">
	                                      								Mutfak Masaları
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Dekoratif Raflar" title="Dekoratif Raflar" class="lazy mddMenuImages" src="/frontend/img/categories/raf.jpg" style="font-weight: bold">
	                                      								Dekoratif Raflar
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Baza ve Karyolalar" title="Baza ve Karyolalar" class="lazy mddMenuImages" src="/frontend/img/categories/karyola.jpg" style="font-weight: bold">
	                                      								Baza ve Karyolalar
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Şifonyer" title="Şifonyer" class="lazy mddMenuImages" src="/frontend/img/categories/sifonyer.jpg" style="font-weight: bold">
	                                      								Şifonyer
																	</a></li>
																</ul>
															</li>
														</ul>
													</div>
													<div class="col-md-3">
														<ul class="sub-menu">
															<li>
																
																<ul class="sub-menu">
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Portmanto ve Askılık" title="Portmanto ve Askılık" class="lazy mddMenuImages" src="/frontend/img/categories/portmanto_ve_askilik.jpg" style="font-weight: bold">
	                                      								Portmanto ve Askılık
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Ofis Koltuğu ve Sandalyeler" title="Ofis Koltuğu ve Sandalyeler" class="lazy mddMenuImages" src="/frontend/img/categories/calisma_sandalyeleri.jpg" style="font-weight: bold">
	                                      								Ofis Koltuğu ve Sandalyeler
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Takı Dolapları" title="Takı Dolapları" class="lazy mddMenuImages" src="/frontend/img/categories/taki_dolaplari.jpg" style="font-weight: bold">
	                                      								Takı Dolapları
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Komodin" title="Komodin" class="lazy mddMenuImages" src="/frontend/img/categories/komodin.jpg" style="font-weight: bold">
	                                      								Komodin
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Mobilya Aksesuarları" title="Mobilya Aksesuarları" class="lazy mddMenuImages" src="/frontend/img/categories/mobilya_aksesuarlari.jpg" style="font-weight: bold">
	                                      								Mobilya Aksesuarları
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Sandalyeler" title="Sandalyeler" class="lazy mddMenuImages" src="/frontend/img/categories/sandalyeler.jpg" style="font-weight: bold">
	                                      								Sandalyeler
																	</a></li>
																</ul>
															</li>
														</ul>
													</div>
													<div class="col-md-3">
														<ul class="sub-menu">
															<li>
																
																<ul class="sub-menu">
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Yataklar" title="Yataklar" class="lazy mddMenuImages" src="/frontend/img/categories/yatak.jpg" style="font-weight: bold">
	                                      								Yataklar
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Sehpalar" title="Sehpalar" class="lazy mddMenuImages" src="/frontend/img/categories/zigon_sehpa.jpg" style="font-weight: bold">
	                                      								Sehpalar
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Ayakkabılık" title="Ayakkabılık" class="lazy mddMenuImages" src="/frontend/img/categories/ayakkabilik.jpg" style="font-weight: bold">
	                                      								Ayakkabılık
																	</a></li>
																
																</ul>
															</li>
														</ul>
													</div>
												</div>
											</div>
										</li>
									</ul>
								</li>
								<li class="dropdown mega-menu-item mega-menu-fullwidth">
									<a class="dropdown-toggle" href="#">
										<span style="width:90px; max-width: 40%;">Ahşap ve İnşaat</span>
										
									</a>
									<ul class="dropdown-menu">
										<li>
											<div class="mega-menu-content">
												<div class="row">
													<div class="col-md-3">
														<ul class="sub-menu">
															<li>
																
																<ul class="sub-menu">
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/gardiroplar.jpg" style="font-weight: bold">
	                                      								Gardıroplar
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/bilgisayar_ve_calisma_masasi.jpg" style="font-weight: bold">
	                                      								Bilgisayar ve Çalışma Masası
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/koltuk_ve_kanepeler.jpg" style="font-weight: bold">
	                                      								Koltuk ve Kanepeler
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/kitaplik.jpg" style="font-weight: bold">
	                                      								Kitaplık
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/tv_unitesi_ve_sehpasi.jpg" style="font-weight: bold">
	                                      								TV Ünitesi ve Sehpası
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/cok_amacli_dolap.jpg" style="font-weight: bold">
	                                      								Çok Amaçlı Dolap
																	</a></li>
																</ul>
															</li>
														</ul>
													</div>
													<div class="col-md-3">
														<ul class="sub-menu">
															<li>
															
																<ul class="sub-menu">
															<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Genç Odası Takımları" title="Genç Odası Takımları" class="lazy mddMenuImages" src="/frontend/img/categories/genc_odasi_takimlari.jpg" style="font-weight: bold">
	                                      								Genç Odası Takımları
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Yatak Odası Takımları" title="Yatak Odası Takımları" class="lazy mddMenuImages" src="/frontend/img/categories/yatak_odasi_takimlari.jpg" style="font-weight: bold">
	                                      								Yatak Odası Takımları
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Mutfak Masaları" title="Mutfak Masaları" class="lazy mddMenuImages" src="/frontend/img/categories/mutfak_masalari.jpg" style="font-weight: bold">
	                                      								Mutfak Masaları
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Dekoratif Raflar" title="Dekoratif Raflar" class="lazy mddMenuImages" src="/frontend/img/categories/raf.jpg" style="font-weight: bold">
	                                      								Dekoratif Raflar
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Baza ve Karyolalar" title="Baza ve Karyolalar" class="lazy mddMenuImages" src="/frontend/img/categories/karyola.jpg" style="font-weight: bold">
	                                      								Baza ve Karyolalar
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Şifonyer" title="Şifonyer" class="lazy mddMenuImages" src="/frontend/img/categories/sifonyer.jpg" style="font-weight: bold">
	                                      								Şifonyer
																	</a></li>
																</ul>
															</li>
														</ul>
													</div>
													<div class="col-md-3">
														<ul class="sub-menu">
															<li>
																
																<ul class="sub-menu">
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Portmanto ve Askılık" title="Portmanto ve Askılık" class="lazy mddMenuImages" src="/frontend/img/categories/portmanto_ve_askilik.jpg" style="font-weight: bold">
	                                      								Portmanto ve Askılık
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Ofis Koltuğu ve Sandalyeler" title="Ofis Koltuğu ve Sandalyeler" class="lazy mddMenuImages" src="/frontend/img/categories/calisma_sandalyeleri.jpg" style="font-weight: bold">
	                                      								Ofis Koltuğu ve Sandalyeler
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Takı Dolapları" title="Takı Dolapları" class="lazy mddMenuImages" src="/frontend/img/categories/taki_dolaplari.jpg" style="font-weight: bold">
	                                      								Takı Dolapları
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Komodin" title="Komodin" class="lazy mddMenuImages" src="/frontend/img/categories/komodin.jpg" style="font-weight: bold">
	                                      								Komodin
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Mobilya Aksesuarları" title="Mobilya Aksesuarları" class="lazy mddMenuImages" src="/frontend/img/categories/mobilya_aksesuarlari.jpg" style="font-weight: bold">
	                                      								Mobilya Aksesuarları
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Sandalyeler" title="Sandalyeler" class="lazy mddMenuImages" src="/frontend/img/categories/sandalyeler.jpg" style="font-weight: bold">
	                                      								Sandalyeler
																	</a></li>
																</ul>
															</li>
														</ul>
													</div>
													<div class="col-md-3">
														<ul class="sub-menu">
															<li>
																
																<ul class="sub-menu">
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Yataklar" title="Yataklar" class="lazy mddMenuImages" src="/frontend/img/categories/yatak.jpg" style="font-weight: bold">
	                                      								Yataklar
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Sehpalar" title="Sehpalar" class="lazy mddMenuImages" src="/frontend/img/categories/zigon_sehpa.jpg" style="font-weight: bold">
	                                      								Sehpalar
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Ayakkabılık" title="Ayakkabılık" class="lazy mddMenuImages" src="/frontend/img/categories/ayakkabilik.jpg" style="font-weight: bold">
	                                      								Ayakkabılık
																	</a></li>
																
																</ul>
															</li>
														</ul>
													</div>
												</div>
											</div>
										</li>
									</ul>
								</li>
								<li class="dropdown mega-menu-item mega-menu-fullwidth">
									<a class="dropdown-toggle" href="#">
										<span style="width:100px; max-width: 40%;">Hırdavat El Aletleri ve Oto</span>
										
									</a>
									<ul class="dropdown-menu">
										<li>
											<div class="mega-menu-content">
												<div class="row">
													<div class="col-md-3">
														<ul class="sub-menu">
															<li>
																
																<ul class="sub-menu">
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/gardiroplar.jpg" style="font-weight: bold">
	                                      								Gardıroplar
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/bilgisayar_ve_calisma_masasi.jpg" style="font-weight: bold">
	                                      								Bilgisayar ve Çalışma Masası
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/koltuk_ve_kanepeler.jpg" style="font-weight: bold">
	                                      								Koltuk ve Kanepeler
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/kitaplik.jpg" style="font-weight: bold">
	                                      								Kitaplık
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/tv_unitesi_ve_sehpasi.jpg" style="font-weight: bold">
	                                      								TV Ünitesi ve Sehpası
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/cok_amacli_dolap.jpg" style="font-weight: bold">
	                                      								Çok Amaçlı Dolap
																	</a></li>
																</ul>
															</li>
														</ul>
													</div>
													<div class="col-md-3">
														<ul class="sub-menu">
															<li>
															
																<ul class="sub-menu">
															<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Genç Odası Takımları" title="Genç Odası Takımları" class="lazy mddMenuImages" src="/frontend/img/categories/genc_odasi_takimlari.jpg" style="font-weight: bold">
	                                      								Genç Odası Takımları
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Yatak Odası Takımları" title="Yatak Odası Takımları" class="lazy mddMenuImages" src="/frontend/img/categories/yatak_odasi_takimlari.jpg" style="font-weight: bold">
	                                      								Yatak Odası Takımları
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Mutfak Masaları" title="Mutfak Masaları" class="lazy mddMenuImages" src="/frontend/img/categories/mutfak_masalari.jpg" style="font-weight: bold">
	                                      								Mutfak Masaları
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Dekoratif Raflar" title="Dekoratif Raflar" class="lazy mddMenuImages" src="/frontend/img/categories/raf.jpg" style="font-weight: bold">
	                                      								Dekoratif Raflar
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Baza ve Karyolalar" title="Baza ve Karyolalar" class="lazy mddMenuImages" src="/frontend/img/categories/karyola.jpg" style="font-weight: bold">
	                                      								Baza ve Karyolalar
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Şifonyer" title="Şifonyer" class="lazy mddMenuImages" src="/frontend/img/categories/sifonyer.jpg" style="font-weight: bold">
	                                      								Şifonyer
																	</a></li>
																</ul>
															</li>
														</ul>
													</div>
													<div class="col-md-3">
														<ul class="sub-menu">
															<li>
																
																<ul class="sub-menu">
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Portmanto ve Askılık" title="Portmanto ve Askılık" class="lazy mddMenuImages" src="/frontend/img/categories/portmanto_ve_askilik.jpg" style="font-weight: bold">
	                                      								Portmanto ve Askılık
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Ofis Koltuğu ve Sandalyeler" title="Ofis Koltuğu ve Sandalyeler" class="lazy mddMenuImages" src="/frontend/img/categories/calisma_sandalyeleri.jpg" style="font-weight: bold">
	                                      								Ofis Koltuğu ve Sandalyeler
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Takı Dolapları" title="Takı Dolapları" class="lazy mddMenuImages" src="/frontend/img/categories/taki_dolaplari.jpg" style="font-weight: bold">
	                                      								Takı Dolapları
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Komodin" title="Komodin" class="lazy mddMenuImages" src="/frontend/img/categories/komodin.jpg" style="font-weight: bold">
	                                      								Komodin
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Mobilya Aksesuarları" title="Mobilya Aksesuarları" class="lazy mddMenuImages" src="/frontend/img/categories/mobilya_aksesuarlari.jpg" style="font-weight: bold">
	                                      								Mobilya Aksesuarları
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Sandalyeler" title="Sandalyeler" class="lazy mddMenuImages" src="/frontend/img/categories/sandalyeler.jpg" style="font-weight: bold">
	                                      								Sandalyeler
																	</a></li>
																</ul>
															</li>
														</ul>
													</div>
													<div class="col-md-3">
														<ul class="sub-menu">
															<li>
																
																<ul class="sub-menu">
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Yataklar" title="Yataklar" class="lazy mddMenuImages" src="/frontend/img/categories/yatak.jpg" style="font-weight: bold">
	                                      								Yataklar
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Sehpalar" title="Sehpalar" class="lazy mddMenuImages" src="/frontend/img/categories/zigon_sehpa.jpg" style="font-weight: bold">
	                                      								Sehpalar
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Ayakkabılık" title="Ayakkabılık" class="lazy mddMenuImages" src="/frontend/img/categories/ayakkabilik.jpg" style="font-weight: bold">
	                                      								Ayakkabılık
																	</a></li>
																
																</ul>
															</li>
														</ul>
													</div>
												</div>
											</div>
										</li>
									</ul>
								</li>
								<li class="dropdown mega-menu-item mega-menu-fullwidth">
									<a class="dropdown-toggle" href="#">
										<span style="width:100px; max-width: 40%;">Elektrikli El Aletleri</span>
										
									</a>
									<ul class="dropdown-menu">
										<li>
											<div class="mega-menu-content">
												<div class="row">
													<div class="col-md-3">
														<ul class="sub-menu">
															<li>
																
																<ul class="sub-menu">
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/gardiroplar.jpg" style="font-weight: bold">
	                                      								Gardıroplar
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/bilgisayar_ve_calisma_masasi.jpg" style="font-weight: bold">
	                                      								Bilgisayar ve Çalışma Masası
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/koltuk_ve_kanepeler.jpg" style="font-weight: bold">
	                                      								Koltuk ve Kanepeler
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/kitaplik.jpg" style="font-weight: bold">
	                                      								Kitaplık
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/tv_unitesi_ve_sehpasi.jpg" style="font-weight: bold">
	                                      								TV Ünitesi ve Sehpası
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/cok_amacli_dolap.jpg" style="font-weight: bold">
	                                      								Çok Amaçlı Dolap
																	</a></li>
																</ul>
															</li>
														</ul>
													</div>
													<div class="col-md-3">
														<ul class="sub-menu">
															<li>
															
																<ul class="sub-menu">
															<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Genç Odası Takımları" title="Genç Odası Takımları" class="lazy mddMenuImages" src="/frontend/img/categories/genc_odasi_takimlari.jpg" style="font-weight: bold">
	                                      								Genç Odası Takımları
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Yatak Odası Takımları" title="Yatak Odası Takımları" class="lazy mddMenuImages" src="/frontend/img/categories/yatak_odasi_takimlari.jpg" style="font-weight: bold">
	                                      								Yatak Odası Takımları
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Mutfak Masaları" title="Mutfak Masaları" class="lazy mddMenuImages" src="/frontend/img/categories/mutfak_masalari.jpg" style="font-weight: bold">
	                                      								Mutfak Masaları
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Dekoratif Raflar" title="Dekoratif Raflar" class="lazy mddMenuImages" src="/frontend/img/categories/raf.jpg" style="font-weight: bold">
	                                      								Dekoratif Raflar
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Baza ve Karyolalar" title="Baza ve Karyolalar" class="lazy mddMenuImages" src="/frontend/img/categories/karyola.jpg" style="font-weight: bold">
	                                      								Baza ve Karyolalar
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Şifonyer" title="Şifonyer" class="lazy mddMenuImages" src="/frontend/img/categories/sifonyer.jpg" style="font-weight: bold">
	                                      								Şifonyer
																	</a></li>
																</ul>
															</li>
														</ul>
													</div>
													<div class="col-md-3">
														<ul class="sub-menu">
															<li>
																
																<ul class="sub-menu">
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Portmanto ve Askılık" title="Portmanto ve Askılık" class="lazy mddMenuImages" src="/frontend/img/categories/portmanto_ve_askilik.jpg" style="font-weight: bold">
	                                      								Portmanto ve Askılık
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Ofis Koltuğu ve Sandalyeler" title="Ofis Koltuğu ve Sandalyeler" class="lazy mddMenuImages" src="/frontend/img/categories/calisma_sandalyeleri.jpg" style="font-weight: bold">
	                                      								Ofis Koltuğu ve Sandalyeler
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Takı Dolapları" title="Takı Dolapları" class="lazy mddMenuImages" src="/frontend/img/categories/taki_dolaplari.jpg" style="font-weight: bold">
	                                      								Takı Dolapları
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Komodin" title="Komodin" class="lazy mddMenuImages" src="/frontend/img/categories/komodin.jpg" style="font-weight: bold">
	                                      								Komodin
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Mobilya Aksesuarları" title="Mobilya Aksesuarları" class="lazy mddMenuImages" src="/frontend/img/categories/mobilya_aksesuarlari.jpg" style="font-weight: bold">
	                                      								Mobilya Aksesuarları
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Sandalyeler" title="Sandalyeler" class="lazy mddMenuImages" src="/frontend/img/categories/sandalyeler.jpg" style="font-weight: bold">
	                                      								Sandalyeler
																	</a></li>
																</ul>
															</li>
														</ul>
													</div>
													<div class="col-md-3">
														<ul class="sub-menu">
															<li>
																
																<ul class="sub-menu">
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Yataklar" title="Yataklar" class="lazy mddMenuImages" src="/frontend/img/categories/yatak.jpg" style="font-weight: bold">
	                                      								Yataklar
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Sehpalar" title="Sehpalar" class="lazy mddMenuImages" src="/frontend/img/categories/zigon_sehpa.jpg" style="font-weight: bold">
	                                      								Sehpalar
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Ayakkabılık" title="Ayakkabılık" class="lazy mddMenuImages" src="/frontend/img/categories/ayakkabilik.jpg" style="font-weight: bold">
	                                      								Ayakkabılık
																	</a></li>
																
																</ul>
															</li>
														</ul>
													</div>
												</div>
											</div>
										</li>
									</ul>
								</li>
								<li class="dropdown mega-menu-item mega-menu-fullwidth">
									<a class="dropdown-toggle" href="#">
										<span>Beyaz Eşya</span>
										
									</a>
									<ul class="dropdown-menu">
										<li>
											<div class="mega-menu-content">
												<div class="row">
													<div class="col-md-3">
														<ul class="sub-menu">
															<li>
																
																<ul class="sub-menu">
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/gardiroplar.jpg" style="font-weight: bold">
	                                      								Gardıroplar
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/bilgisayar_ve_calisma_masasi.jpg" style="font-weight: bold">
	                                      								Bilgisayar ve Çalışma Masası
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/koltuk_ve_kanepeler.jpg" style="font-weight: bold">
	                                      								Koltuk ve Kanepeler
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/kitaplik.jpg" style="font-weight: bold">
	                                      								Kitaplık
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/tv_unitesi_ve_sehpasi.jpg" style="font-weight: bold">
	                                      								TV Ünitesi ve Sehpası
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Gardıroplar" title="Gardıroplar" class="lazy mddMenuImages" src="/frontend/img/categories/cok_amacli_dolap.jpg" style="font-weight: bold">
	                                      								Çok Amaçlı Dolap
																	</a></li>
																</ul>
															</li>
														</ul>
													</div>
													<div class="col-md-3">
														<ul class="sub-menu">
															<li>
															
																<ul class="sub-menu">
															<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Genç Odası Takımları" title="Genç Odası Takımları" class="lazy mddMenuImages" src="/frontend/img/categories/genc_odasi_takimlari.jpg" style="font-weight: bold">
	                                      								Genç Odası Takımları
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Yatak Odası Takımları" title="Yatak Odası Takımları" class="lazy mddMenuImages" src="/frontend/img/categories/yatak_odasi_takimlari.jpg" style="font-weight: bold">
	                                      								Yatak Odası Takımları
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Mutfak Masaları" title="Mutfak Masaları" class="lazy mddMenuImages" src="/frontend/img/categories/mutfak_masalari.jpg" style="font-weight: bold">
	                                      								Mutfak Masaları
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Dekoratif Raflar" title="Dekoratif Raflar" class="lazy mddMenuImages" src="/frontend/img/categories/raf.jpg" style="font-weight: bold">
	                                      								Dekoratif Raflar
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Baza ve Karyolalar" title="Baza ve Karyolalar" class="lazy mddMenuImages" src="/frontend/img/categories/karyola.jpg" style="font-weight: bold">
	                                      								Baza ve Karyolalar
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Şifonyer" title="Şifonyer" class="lazy mddMenuImages" src="/frontend/img/categories/sifonyer.jpg" style="font-weight: bold">
	                                      								Şifonyer
																	</a></li>
																</ul>
															</li>
														</ul>
													</div>
													<div class="col-md-3">
														<ul class="sub-menu">
															<li>
																
																<ul class="sub-menu">
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Portmanto ve Askılık" title="Portmanto ve Askılık" class="lazy mddMenuImages" src="/frontend/img/categories/portmanto_ve_askilik.jpg" style="font-weight: bold">
	                                      								Portmanto ve Askılık
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Ofis Koltuğu ve Sandalyeler" title="Ofis Koltuğu ve Sandalyeler" class="lazy mddMenuImages" src="/frontend/img/categories/calisma_sandalyeleri.jpg" style="font-weight: bold">
	                                      								Ofis Koltuğu ve Sandalyeler
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Takı Dolapları" title="Takı Dolapları" class="lazy mddMenuImages" src="/frontend/img/categories/taki_dolaplari.jpg" style="font-weight: bold">
	                                      								Takı Dolapları
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Komodin" title="Komodin" class="lazy mddMenuImages" src="/frontend/img/categories/komodin.jpg" style="font-weight: bold">
	                                      								Komodin
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Mobilya Aksesuarları" title="Mobilya Aksesuarları" class="lazy mddMenuImages" src="/frontend/img/categories/mobilya_aksesuarlari.jpg" style="font-weight: bold">
	                                      								Mobilya Aksesuarları
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Sandalyeler" title="Sandalyeler" class="lazy mddMenuImages" src="/frontend/img/categories/sandalyeler.jpg" style="font-weight: bold">
	                                      								Sandalyeler
																	</a></li>
																</ul>
															</li>
														</ul>
													</div>
													<div class="col-md-3">
														<ul class="sub-menu">
															<li>
																
																<ul class="sub-menu">
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Yataklar" title="Yataklar" class="lazy mddMenuImages" src="/frontend/img/categories/yatak.jpg" style="font-weight: bold">
	                                      								Yataklar
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Sehpalar" title="Sehpalar" class="lazy mddMenuImages" src="/frontend/img/categories/zigon_sehpa.jpg" style="font-weight: bold">
	                                      								Sehpalar
																	</a></li>
																	<li><a class="singlemenu" href="feature-pricing-tables.html">
																		<img width="52" height="54" alt="Ayakkabılık" title="Ayakkabılık" class="lazy mddMenuImages" src="/frontend/img/categories/ayakkabilik.jpg" style="font-weight: bold">
	                                      								Ayakkabılık
																	</a></li>
																
																</ul>
															</li>
														</ul>
													</div>
												</div>
											</div>
										</li>
									</ul>
								</li>
								
							</ul>
						</nav>
					</div>
				</div>
			</header>
							@yield('content')

			<footer id="footer">
				<div class="container">
					<div class="row">
						<div class="footer-ribbon">
							<span>Get in Touch</span>
						</div>
						<div class="col-md-3">
							<div class="newsletter">
								<h4>Newsletter</h4>
								<p>Keep up on our always evolving product features and technology. Enter your e-mail and subscribe to our newsletter.</p>
			
								<div class="alert alert-success hidden" id="newsletterSuccess">
									<strong>Success!</strong> You've been added to our email list.
								</div>
			
								<div class="alert alert-danger hidden" id="newsletterError"></div>
			
								<form id="newsletterForm" action="php/newsletter-subscribe.php" method="POST">
									<div class="input-group">
										<input class="form-control" placeholder="Email Address" name="newsletterEmail" id="newsletterEmail" type="text">
										<span class="input-group-btn">
											<button class="btn btn-default" type="submit">Go!</button>
										</span>
									</div>
								</form>
							</div>
						</div>
						<div class="col-md-3">
							<h4>Latest Tweets</h4>
							<div id="tweet" class="twitter" data-plugin-tweets data-plugin-options='{"username": "", "count": 2}'>
								<p>Please wait...</p>
							</div>
						</div>
						<div class="col-md-4">
							<div class="contact-details">
								<h4>Contact Us</h4>
								<ul class="contact">
									<li><p><i class="fa fa-map-marker"></i> <strong>Address:</strong> 1234 Street Name, City Name, United States</p></li>
									<li><p><i class="fa fa-phone"></i> <strong>Phone:</strong> (123) 456-7890</p></li>
									<li><p><i class="fa fa-envelope"></i> <strong>Email:</strong> <a href="mailto:mail@example.com">mail@example.com</a></p></li>
								</ul>
							</div>
						</div>
						<div class="col-md-2">
							<h4>Follow Us</h4>
							<div class="social-icons">
								<ul class="social-icons">
									<li class="facebook"><a href="http://www.facebook.com/" target="_blank" data-placement="bottom" data-tooltip title="Facebook">Facebook</a></li>
									<li class="twitter"><a href="http://www.twitter.com/" target="_blank" data-placement="bottom" data-tooltip title="Twitter">Twitter</a></li>
									<li class="linkedin"><a href="http://www.linkedin.com/" target="_blank" data-placement="bottom" data-tooltip title="Linkedin">Linkedin</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="footer-copyright">
					<div class="container">
						<div class="row">
							<div class="col-md-1">
								<a href="index.html" class="logo">
									<img alt="Porto Website Template" class="img-responsive" src="img/logo-footer.png">
								</a>
							</div>
							<div class="col-md-7">
								<p>© Copyright 2015. All Rights Reserved.</p>
							</div>
							<div class="col-md-4">
								<nav id="sub-menu">
									<ul>
										<li><a href="page-faq.html">FAQ's</a></li>
										<li><a href="sitemap.html">Sitemap</a></li>
										<li><a href="contact-us.html">Contact</a></li>
									</ul>
								</nav>
							</div>
						</div>
					</div>
				</div>
			</footer>
		</div>

		<!-- Vendor -->
		<script  type="text/javascript" src="{{ URL::to('') }}/frontend/vendor/jquery/jquery.js"></script>
		<script type="text/javascript" src="{{ URL::to('') }}/frontend/vendor/jquery.appear/jquery.appear.js"></script>
		<!--<script type="text/javascript" src="{{ URL::to('') }}/frontend/vendor/jquery.easing/jquery.easing.js"></script>-->
		<script type="text/javascript" src="{{ URL::to('') }}/frontend/vendor/jquery-cookie/jquery-cookie.js"></script>
		<script type="text/javascript" src="{{ URL::to('') }}/frontend/vendor/bootstrap/bootstrap.js"></script>
		<script type="text/javascript" src="{{ URL::to('') }}/frontend/vendor/common/common.js"></script>
		<script type="text/javascript" src="{{ URL::to('') }}/frontend/vendor/jquery.validation/jquery.validation.js"></script>
		<script type="text/javascript" src="{{ URL::to('') }}/frontend/vendor/jquery.stellar/jquery.stellar.js"></script>
		<script type="text/javascript" src="{{ URL::to('') }}/frontend/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.js"></script>
		<script type="text/javascript"  src="{{ URL::to('') }}/frontend/vendor/jquery.gmap/jquery.gmap.js"></script>
		<script type="text/javascript" src="{{ URL::to('') }}/frontend/vendor/isotope/jquery.isotope.js"></script>
		<script type="text/javascript" src="{{ URL::to('') }}/frontend/vendor/owlcarousel/owl.carousel.js"></script>
		<script type="text/javascript" src="{{ URL::to('') }}/frontend/vendor/jflickrfeed/jflickrfeed.js"></script>
		<script type="text/javascript" src="{{ URL::to('') }}/frontend/vendor/magnific-popup/jquery.magnific-popup.js"></script>
		<script type="text/javascript" src="{{ URL::to('') }}/frontend/vendor/vide/vide.js"></script>


		
		<!-- Theme Base, Components and Settings -->
		<script type="text/javascript" src="{{ URL::to('') }}/frontend/js/theme.js"></script>
		
		<!-- Specific Page Vendor and Views -->
		<script type="text/javascript" src="{{ URL::to('') }}/frontend/vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
		<script type="text/javascript" src="{{ URL::to('') }}/frontend/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
		<script type="text/javascript" src="{{ URL::to('') }}/frontend/vendor/circle-flip-slideshow/js/jquery.flipshow.js"></script>
		<script type="text/javascript" src="{{ URL::to('') }}/frontend/js/views/view.home.js"></script>
		
		<!-- Theme Custom -->
		<script type="text/javascript" src="{{ URL::to('') }}/frontend/js/custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script type="text/javascript" src="{{ URL::to('') }}/frontend/js/theme.init.js"></script>

		<!-- eklenenen kısımlar -->
		<script type="text/javascript" src="{{ URL::to('') }}/frontend/themes/js/jquery.js" type="text/javascript"></script>
	<script type="text/javascript" src="{{ URL::to('') }}/frontend/themes/js/bootstrap.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="{{ URL::to('') }}/frontend/themes/js/google-code-prettify/prettify.js"></script>
	
	<script type="text/javascript" src="{{ URL::to('') }}/frontend/themes/js/bootshop.js"></script>
    <script type="text/javascript" src="{{ URL::to('') }}/frontend/themes/js/jquery.lightbox-0.5.js"></script>

		<!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information.
		<script type="text/javascript">
		
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-12345678-1']);
			_gaq.push(['_trackPageview']);
		
			(function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();
		
		</script>
		 -->
		 @yield('js')
	</body>
</html>