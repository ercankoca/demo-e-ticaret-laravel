<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/clear', function () {
    Artisan::call('cache:clear');
    Artisan::call('route:clear');
    Artisan::call('config:clear');
});

Route::get('/', function () {
    return view('front/index');
});

Route::get('/mobilya/gardiroplar', function()
{
	return view('front/mobilya/gardiroplar');
});

Route::get('/mobilya/gardiroplar/surgulu-gardiroplar', function()
{
	return view('front/mobilya/surgulu_gardiroplar');
});
Route::get('/gardiroplar/surgulu-gardiroplar/surgulu-gardiroplar-urun1', function()
{
	return view('front/mobilya/surgulu_gardiroplar_urun1');
});

Route::get('/mobilya/gardiroplar/kapakli-gardiroplar', function()
{
	return view('front/mobilya/kapakli_gardiroplar');
});

Route::get('/gardiroplar/kapakli-gardiroplar/kapakli-gardiroplar-urun1', function()
{
	return view('front/mobilya/kapakli_gardiroplar_urun1');
});


Auth::routes();
Route::get('/logout', 'Auth\LoginController@logout');
Route::group(['prefix' => 'panel', 'middleware' => ['auth','admin']], function () {
    
     Route::get('/', function () {
        return view('admin.index');
    })->name('admin.index');

     Route::resource('category','Admin\CategoriesController');
     Route::resource('product','Admin\ProductsController');
     Route::get('product/image/{productId}','Admin\ProductsController@image');
     Route::get('product/image/deleteImage/{productId}','Admin\ProductsController@DeleteImage');
     Route::post('product/image-upload/{productId}','Admin\ProductsController@uploadImages');

    });



